package com.example.xyrestocustomerapp.search.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.xyrestocustomerapp.search.repository.SearchProductRepository

class SearchProductViewModelFactory(private val searchProductRepository: SearchProductRepository): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SearchProductViewModel(searchProductRepository) as T
    }
}