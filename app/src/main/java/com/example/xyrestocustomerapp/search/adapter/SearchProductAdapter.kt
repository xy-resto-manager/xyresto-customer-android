package com.example.xyrestocustomerapp.search.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners
import com.example.xyrestocustomerapp.R
import com.example.xyrestocustomerapp.base.adapter.FooterAdapter
import com.example.xyrestocustomerapp.base.adapter.LoadingHolder
import com.example.xyrestocustomerapp.base.constant.Constant.SERVER_URL
import com.example.xyrestocustomerapp.base.model.Product
import com.example.xyrestocustomerapp.base.utils.Utils.formatRp
import com.example.xyrestocustomerapp.databinding.MenuProductItemBinding

class SearchProductAdapter(
    private val onProductAddClicked: (Product) -> Unit
): FooterAdapter<Product>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseRvViewHolder<Product> {
        when(viewType){
            VIEW_TYPE_CONTENT -> {
                val viewBinding = MenuProductItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                return MenuProductHolder(viewBinding)
            }
            else -> {
                LayoutInflater.from(parent.context).inflate(R.layout.loading_item, parent, false)
                    .let {
                        return LoadingHolder(it)
                    }
            }
        }
    }

    override fun onBindViewHolder(holder: BaseRvViewHolder<Product>, position: Int) {
        mutableItems[position%mutableItemCount].let(holder::bind)
    }

    override fun getItemViewType(position: Int): Int {
        return if (isFooterVisible && position == mutableItemCount -1) {
            VIEW_TYPE_FOOTER
        } else {
            VIEW_TYPE_CONTENT
        }
    }

    inner class MenuProductHolder(
        private val viewBinding: MenuProductItemBinding
    ): BaseRvViewHolder<Product>(viewBinding.root){
        override fun bind(data: Product, position: Int?) {
            with(viewBinding){
                ivProduct.layout(0,0,0,0)
                Glide.with(root.context)
                    .load(SERVER_URL+data.imageUrl)
                    .transform(CenterCrop(), GranularRoundedCorners(20.0F, 0.0F, 0.0F, 20.0F))
                    .placeholder(R.drawable.ic_food)
                    .into(ivProduct)
                tvProductTitle.text = data.name
                tvProductDesc.text = data.description
                tvProductPrice.text = data.price.formatRp()
                ivBtnAdd.setOnClickListener {
                    onProductAddClicked(data)
                }
            }
        }
    }
}