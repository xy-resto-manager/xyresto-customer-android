package com.example.xyrestocustomerapp.search.repository

import com.example.xyrestocustomerapp.search.network.ISearchProductService

class SearchProductRepository(private val iProductApiService: ISearchProductService) {
    suspend fun getProductByQuery(query: String? = null, page: Int = 1) = iProductApiService.getProducts(query = query, page = page)
}