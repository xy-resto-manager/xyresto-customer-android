package com.example.xyrestocustomerapp.search.network

import com.example.xyrestocustomerapp.base.model.BaseApiResponse
import com.example.xyrestocustomerapp.base.model.Product
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ISearchProductService {
    @GET("product/products")
    suspend fun getProducts(
        @Query("query") query: String? = null,
        @Query("page") page: Int? = 1
    ): Response<BaseApiResponse<List<Product>>>
}