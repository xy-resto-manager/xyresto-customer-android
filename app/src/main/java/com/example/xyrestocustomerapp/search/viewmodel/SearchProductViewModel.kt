package com.example.xyrestocustomerapp.search.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.xyrestocustomerapp.base.model.BaseApiResponse
import com.example.xyrestocustomerapp.base.model.Product
import com.example.xyrestocustomerapp.base.utils.ApiCallHelper.safeApiCall
import com.example.xyrestocustomerapp.base.utils.ResponseWrapper
import com.example.xyrestocustomerapp.search.repository.SearchProductRepository
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SearchProductViewModel(private val productRepository: SearchProductRepository): ViewModel() {

    private val _searchProductLiveData = MutableLiveData<ResponseWrapper<BaseApiResponse<List<Product>>>>()
    val searchProductLiveData : LiveData<ResponseWrapper<BaseApiResponse<List<Product>>>>
        get() = _searchProductLiveData
    private var debouncePeriod: Long = 800
    private var searchJob: Job? = null

    fun searchProductByQuery(query: String, page: Int = 1){
        searchJob?.cancel()
        searchJob =viewModelScope.launch {
            if (query.length > 3) {
                delay(debouncePeriod)
                val result = safeApiCall { productRepository.getProductByQuery(query=query, page=page) }
                _searchProductLiveData.postValue(result)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        searchJob?.cancel()
    }
}