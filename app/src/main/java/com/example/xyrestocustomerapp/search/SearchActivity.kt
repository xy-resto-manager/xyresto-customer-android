package com.example.xyrestocustomerapp.search

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.xyrestocustomerapp.base.listener.PaginationListener
import com.example.xyrestocustomerapp.base.model.Product
import com.example.xyrestocustomerapp.base.storage.network.ApiClient
import com.example.xyrestocustomerapp.base.storage.session.ProductSharedPreference
import com.example.xyrestocustomerapp.base.utils.ResponseWrapper
import com.example.xyrestocustomerapp.base.utils.Utils
import com.example.xyrestocustomerapp.base.utils.Utils.orDefaultInt
import com.example.xyrestocustomerapp.base.widget.decoration.SpacingItemDecoration
import com.example.xyrestocustomerapp.cart.model.CartRequest
import com.example.xyrestocustomerapp.cart.network.ICartApiService
import com.example.xyrestocustomerapp.cart.repository.CartRepository
import com.example.xyrestocustomerapp.cart.viewmodel.UpdateCartViewModel
import com.example.xyrestocustomerapp.cart.viewmodel.UpdateCartViewModelFactory
import com.example.xyrestocustomerapp.databinding.ActivitySearchBinding
import com.example.xyrestocustomerapp.search.adapter.SearchProductAdapter
import com.example.xyrestocustomerapp.search.network.ISearchProductService
import com.example.xyrestocustomerapp.search.repository.SearchProductRepository
import com.example.xyrestocustomerapp.search.viewmodel.SearchProductViewModel
import com.example.xyrestocustomerapp.search.viewmodel.SearchProductViewModelFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class SearchActivity : AppCompatActivity(), CoroutineScope {
    private lateinit var viewBinding: ActivitySearchBinding
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job
    private lateinit var job: Job
    private lateinit var productSharedPreference: ProductSharedPreference
    private val productViewModel: SearchProductViewModel by viewModels {
        SearchProductViewModelFactory(
            SearchProductRepository(
                ApiClient.getRetrofitInstance(this).create(ISearchProductService::class.java)
            )
        )
    }
    private val cartViewModel: UpdateCartViewModel by viewModels{ UpdateCartViewModelFactory(CartRepository(
        ApiClient.getRetrofitInstance(this).create(ICartApiService::class.java)))
    }
    private val productListAdapter by lazy { SearchProductAdapter(::onAddProductClicked) }
    private var totalPageProductList: Int = 1
    private var currentPageProductList: Int = 0
    private var isLastPageProductList = false
    private var isLoadingProductList = false
    private var query = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivitySearchBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)
        job = Job()
        productSharedPreference = ProductSharedPreference(this)
        initView()
        initRecyclerView()
        initObserver()
        setPaginationOnProductList()
    }

    private fun initView(){
        with(viewBinding.layoutActionBar){
            setSupportActionBar(toolbar)
            toolbar.title = ""
            ivBtnBack.setOnClickListener {
                onBackPressed()
            }
            etSearch.addTextChangedListener(object : TextWatcher{
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {}

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

                override fun afterTextChanged(s: Editable?) {
                    query = s.toString()
                    productViewModel.searchProductByQuery(s.toString())
                }

            })
            query = etSearch.text.toString()
        }
    }

    private fun initRecyclerView(){
        with(viewBinding.rvProductList){
            layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
            adapter = productListAdapter.apply {
                addItemDecoration(SpacingItemDecoration(topMargin = 40, leftMargin = 35, rightMargin = 35))
            }
        }
    }

    private fun initObserver(){
        productViewModel.searchProductLiveData.observe(this, {
            when(it.status){
                ResponseWrapper.Status.LOADING -> {

                }
                ResponseWrapper.Status.SUCCESS -> {
                    it.body?.pagination?.let {
                        currentPageProductList = it.currentPage
                        totalPageProductList = Utils.ceilInt(
                            it.totalItems.orDefaultInt(1),
                            it.itemsPerPage.orDefaultInt(1)
                        )
                    }
                    if (currentPageProductList < totalPageProductList) {
                        it.body?.data?.let { productListAdapter.setItemList(it, false) }
                        productListAdapter.addFooter(Product(), false)
                        productListAdapter.notifyDataSetChanged()
                    } else {
                        isLastPageProductList = true
                        productListAdapter.removeFooter(false)
                        it.body?.data?.let { productListAdapter.setItemList(it) }
                    }
                }
                else -> {
                    Toast.makeText(this, "ERROR: "+it.body?.errors, Toast.LENGTH_SHORT).show()
                }
            }
            isLoadingProductList = false
        })
        cartViewModel.cartResponse.observe(this, { event ->
            event.getContentIfNotHandled()?.let {
                when(it.status){
                    ResponseWrapper.Status.LOADING -> {

                    }
                    ResponseWrapper.Status.SUCCESS -> {
                        Toast.makeText(this, "Added Into Cart", Toast.LENGTH_SHORT).show()
                    }
                    else -> {
                        Toast.makeText(this, "ERROR: "+ it.body?.errors, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        })
    }

    private fun setPaginationOnProductList(){
        with(viewBinding.rvProductList){
            layoutManager?.let {
                addOnScrollListener(object: PaginationListener(it, 10){
                    override fun loadMoreItems() {
                        isLoadingProductList = true
                        productViewModel.searchProductByQuery(query = query, page = currentPageProductList+1)
                    }

                    override fun isLastPage(): Boolean {
                        return isLastPageProductList
                    }

                    override fun isLoading(): Boolean {
                        return isLoadingProductList
                    }

                })
            }
        }
    }

    private fun onAddProductClicked(product: Product){
        launch(Dispatchers.IO) { productSharedPreference.saveData(product) }
        cartViewModel.callUpdateCurrentCartApi(CartRequest(product.id, 1))
    }

    override fun onDestroy() {
        job.cancel()
        super.onDestroy()
    }
}