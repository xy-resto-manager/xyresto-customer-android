package com.example.xyrestocustomerapp.cart.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CartRequest(
    @field:SerializedName("productId")
    val productId: Int? = null,
    @field:SerializedName("quantity")
    val quantity: Int? = null
): Parcelable