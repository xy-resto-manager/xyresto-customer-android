package com.example.xyrestocustomerapp.cart.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CartProductCategory(
    @field:SerializedName("id")
    val categoryId: Int? = null,
    @field:SerializedName("name")
    val name: String? = null
): Parcelable