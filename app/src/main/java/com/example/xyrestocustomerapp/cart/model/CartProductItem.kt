package com.example.xyrestocustomerapp.cart.model

import android.os.Parcelable
import com.example.xyrestocustomerapp.base.model.Category
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CartProductItem(
    @field:SerializedName("id")
    val productId: Int? = null,
    @field:SerializedName("name")
    val name: String? = null,
    @field:SerializedName("price")
    val price: Int? = null,
    @field:SerializedName("description")
    val description: String? = null,
    @field:SerializedName("imageUrl")
    val imageUrl: String? = null,
    @field:SerializedName("purchaseCount")
    val purchaseCount: Long? = null,
    @field:SerializedName("traits")
    val traits: List<String>? = null,
    @field:SerializedName("category")
    val category: CartProductCategory? = null
): Parcelable

