package com.example.xyrestocustomerapp.cart.model

import android.os.Parcelable
import com.example.xyrestocustomerapp.base.model.Product
import com.example.xyrestocustomerapp.base.model.User
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CartResponse(
    @field:SerializedName("id")
    val id: Int? = null,
    @field:SerializedName("user")
    val user: User? = null,
    @field:SerializedName("summary")
    val summary: CartSummary? = null,
    @field:SerializedName("products")
    val products: List<CartProduct>? = null
): Parcelable
