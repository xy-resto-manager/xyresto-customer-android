package com.example.xyrestocustomerapp.cart.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CartSummary(
    @field:SerializedName("totalPrice")
    val totalPrice: Int = 0,
    @field:SerializedName("quantity")
    val quantity: Int = 0
):Parcelable
