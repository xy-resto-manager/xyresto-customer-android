package com.example.xyrestocustomerapp.cart.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.xyrestocustomerapp.cart.repository.CartRepository

class UpdateCartViewModelFactory(private val repository: CartRepository): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return UpdateCartViewModel(repository) as T
    }
}