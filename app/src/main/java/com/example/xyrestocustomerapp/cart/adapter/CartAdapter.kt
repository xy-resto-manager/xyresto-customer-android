package com.example.xyrestocustomerapp.cart.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners
import com.example.xyrestocustomerapp.R
import com.example.xyrestocustomerapp.base.adapter.BaseRvAdapter
import com.example.xyrestocustomerapp.base.adapter.FooterAdapter
import com.example.xyrestocustomerapp.base.constant.Constant.SERVER_URL
import com.example.xyrestocustomerapp.base.utils.Utils.formatRp
import com.example.xyrestocustomerapp.base.utils.Utils.orDefaultInt
import com.example.xyrestocustomerapp.cart.ICartSummaryCommunicator
import com.example.xyrestocustomerapp.cart.model.CartProduct
import com.example.xyrestocustomerapp.databinding.CartInfoItemBinding
import com.example.xyrestocustomerapp.databinding.CartProductItemBinding

class CartAdapter(
    private val onProductChangeClicked: (CartProduct, Int) -> Unit,
    private val onReserveButtonClicked: () -> Unit,
    private val iCartSummaryCommunicator: ICartSummaryCommunicator
): FooterAdapter<CartProduct>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseRvViewHolder<CartProduct> {
        return when(viewType) {
            VIEW_TYPE_CONTENT -> {
                val cartItemBinding = CartProductItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                CartViewHolder(cartItemBinding)
            }
            else -> {
                val infoItemBinding = CartInfoItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                OtherTypeHolder(infoItemBinding)
            }
        }
    }

    override fun onBindViewHolder(holder: BaseRvViewHolder<CartProduct>, position: Int) {
        mutableItems[position % mutableItemCount].let(holder::bind)
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == mutableItemCount-1) {
            VIEW_TYPE_FOOTER
        } else {
            VIEW_TYPE_CONTENT
        }
    }

    inner class CartViewHolder(
        private val binding: CartProductItemBinding
    ): BaseRvViewHolder<CartProduct>(binding.root){
        override fun bind(data: CartProduct, position: Int?) {
            with(binding){
                ivProduct.layout(0,0,0,0)
                Glide.with(root.context)
                    .load(SERVER_URL+data.product?.imageUrl)
                    .transform(CenterCrop(), GranularRoundedCorners(20.0F, 0.0F, 0.0F, 20.0F))
                    .placeholder(R.drawable.ic_food)
                    .into(ivProduct)
                tvProductTitle.text = data.product?.name
                tvProductPrice.text = data.product?.price.orDefaultInt(0).formatRp()
                tvProductQty.text = data.quantity.toString()

                ivBtnMinus.setOnClickListener {
                    val currentCount = Integer.parseInt(tvProductQty.text.toString()) - 1
                    onProductChangeClicked(data, currentCount)
                }

                ivBtnAdd.setOnClickListener {
                    val currentCount = Integer.parseInt(tvProductQty.text.toString()) + 1
                    onProductChangeClicked(data, currentCount)
                }

                ivBtnClose.setOnClickListener {
                    onProductChangeClicked(data, 0)
                }

            }
        }
    }

    inner class OtherTypeHolder(
        private val binding: CartInfoItemBinding
    ): BaseRvViewHolder<CartProduct>(binding.root){
        override fun bind(data: CartProduct, position: Int?) {
            with(binding){
                val context = root.context
                val quantity = iCartSummaryCommunicator.getCartSummary()?.quantity
                val totalPrice = iCartSummaryCommunicator.getCartSummary()?.totalPrice
                tvProductCount.text = context.resources.getQuantityString(R.plurals.items,
                    quantity.orDefaultInt(), quantity.orDefaultInt())
                tvTotalPrice.text = totalPrice.formatRp()
                btnReserve.text = data.product?.name
                btnReserve.setOnClickListener{
                    onReserveButtonClicked()
                }
            }
        }
    }
}