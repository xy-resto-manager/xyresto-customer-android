package com.example.xyrestocustomerapp.cart.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.xyrestocustomerapp.base.model.BaseApiResponse
import com.example.xyrestocustomerapp.base.model.Event
import com.example.xyrestocustomerapp.base.utils.ApiCallHelper.safeApiCall
import com.example.xyrestocustomerapp.base.utils.ResponseWrapper
import com.example.xyrestocustomerapp.base.utils.Utils.orDefaultInt
import com.example.xyrestocustomerapp.cart.model.CartRequest
import com.example.xyrestocustomerapp.cart.model.CartResponse
import com.example.xyrestocustomerapp.cart.repository.CartRepository
import com.example.xyrestocustomerapp.reservation.ReservationStatus
import com.example.xyrestocustomerapp.reservation.model.ReservationResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CartViewModel(private val cartRepository: CartRepository): ViewModel() {
    private val _cartResponse = MutableLiveData<ResponseWrapper<BaseApiResponse<CartResponse>>>()
    val cartResponse: LiveData<ResponseWrapper<BaseApiResponse<CartResponse>>> get() = _cartResponse
    private val _diningReservationResponse = MutableLiveData<Event<ResponseWrapper<BaseApiResponse<Int?>>>>()
    val diningReservationResponse: LiveData<Event<ResponseWrapper<BaseApiResponse<Int?>>>> get() = _diningReservationResponse
    private val _addOrderToReservation = MutableLiveData<Event<ResponseWrapper<BaseApiResponse<ReservationResponse>>>>()
    val addOrderToReservation: LiveData<Event<ResponseWrapper<BaseApiResponse<ReservationResponse>>>> get() = _addOrderToReservation

    init {
        getDiningReservation()
    }

    fun callUpdateCurrentCartApi(cartRequest: CartRequest)  {
        viewModelScope.launch(Dispatchers.IO) {
            val result = safeApiCall { cartRepository.callUpdateCartApi(cartRequest=cartRequest) }
            _cartResponse.postValue(result)
        }
    }

    fun getDiningReservation(){
        viewModelScope.launch(Dispatchers.IO){
            safeApiCall { cartRepository.getAllReservation() }.let {
                _diningReservationResponse.postValue(
                    Event(ResponseWrapper(
                        it.status,
                        BaseApiResponse(
                            it.body?.code,
                            it.body?.data?.firstOrNull { it.status == ReservationStatus.DINING }?.id,
                            it.body?.status,
                            it.body?.errors,
                            it.body?.pagination
                        ),
                        it.message
                    ))
                )
            }
        }
    }

    fun addOrderToReservation(reservationId: Int){
        viewModelScope.launch(Dispatchers.IO) {
            _cartResponse.value?.body?.data?.products?.let {
                val cartRequest = mutableListOf<CartRequest>()
                it.forEach {
                    cartRequest.add(CartRequest(it.product?.productId, it.quantity))
                }
                _addOrderToReservation.postValue(
                    Event(safeApiCall { cartRepository.addOrderToReservation(reservationId, cartRequest) })
                )
            }
        }
    }

    fun callGetCurrentCartApi() {
        viewModelScope.launch(Dispatchers.IO) {
            val result = safeApiCall { cartRepository.callGetCurrentCartApi() }
            _cartResponse.postValue(result)
        }
    }
}