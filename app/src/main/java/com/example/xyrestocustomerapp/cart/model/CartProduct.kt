package com.example.xyrestocustomerapp.cart.model

import android.os.Parcelable
import com.example.xyrestocustomerapp.base.model.Product
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CartProduct(
    @field:SerializedName("product")
    val product: CartProductItem? = null,
    @field:SerializedName("quantity")
    val quantity: Int? = null
): Parcelable
