package com.example.xyrestocustomerapp.cart.view

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.xyrestocustomerapp.R
import com.example.xyrestocustomerapp.base.storage.network.ApiClient
import com.example.xyrestocustomerapp.base.utils.ResponseWrapper
import com.example.xyrestocustomerapp.base.widget.decoration.SpacingItemDecoration
import com.example.xyrestocustomerapp.cart.ICartSummaryCommunicator
import com.example.xyrestocustomerapp.cart.adapter.CartAdapter
import com.example.xyrestocustomerapp.cart.model.CartProduct
import com.example.xyrestocustomerapp.cart.model.CartProductItem
import com.example.xyrestocustomerapp.cart.model.CartRequest
import com.example.xyrestocustomerapp.cart.model.CartSummary
import com.example.xyrestocustomerapp.cart.network.ICartApiService
import com.example.xyrestocustomerapp.cart.repository.CartRepository
import com.example.xyrestocustomerapp.cart.viewmodel.CartViewModel
import com.example.xyrestocustomerapp.cart.viewmodel.CartViewModelFactory
import com.example.xyrestocustomerapp.databinding.FragmentCartBinding
import com.example.xyrestocustomerapp.home.IRetryCallBack
import com.example.xyrestocustomerapp.home.LoadingCommunicator
import com.example.xyrestocustomerapp.home.LoadingMode

class CartFragment : Fragment(), ICartSummaryCommunicator {
    private var _viewBinding: FragmentCartBinding? = null
    private val viewBinding get() = _viewBinding!!
    private val cartViewModel: CartViewModel by viewModels{
        CartViewModelFactory(CartRepository(ApiClient.getRetrofitInstance(requireContext()).create(ICartApiService::class.java)))
    }
    private val cartAdapter = CartAdapter(::onCartItemChanged, ::onReserveButtonClicked, this)
    private var cartSummary: CartSummary? = null
    private var diningReservationId: Int? = null
    private val iLoadingCommunicator: LoadingCommunicator by lazy { activity as LoadingCommunicator }
    private val retryCallbackList = mutableListOf<Pair<Int, IRetryCallBack>>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _viewBinding = FragmentCartBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        initObserver()
    }

    private fun initObserver(){
        iLoadingCommunicator.startLoading(2, LoadingMode.PROGRESS_BAR_WITH_BACKGROUND)
        cartViewModel.diningReservationResponse.observe(viewLifecycleOwner, { event ->
            retryCallbackList.clear()
            event.peekContent().let {
                when(it.status){
                    ResponseWrapper.Status.SUCCESS -> {
                        diningReservationId = it.body?.data
                        if (!event.hasBeenHandled)
                            cartViewModel.callGetCurrentCartApi()
                        retryCallbackList.add(Pair(1, object : IRetryCallBack{
                            override fun retry() {
                                cartViewModel.callGetCurrentCartApi()
                            }
                        }))
                        iLoadingCommunicator.updateLoadingProgress()
                    }
                    else -> {
                        with(iLoadingCommunicator){
                            if (getProgress().second == 2){
                                updateLoadingProgress()
                                updateLoadingProgress()
                            } else {
                                updateLoadingProgress()
                            }
                            retryCallbackList.add(Pair(0, object : IRetryCallBack{
                                override fun retry() {
                                    cartViewModel.getDiningReservation()
                                }
                            }))
                            if (getProgress().first == getProgress().second){
                                iLoadingCommunicator.onError(it.body?.errors.toString(), retryCallbackList)
                            }
                        }
                    }
                }
            }
        })
        cartViewModel.cartResponse.observe(viewLifecycleOwner, {
            when(it.status){
                ResponseWrapper.Status.LOADING -> {
                    Log.d("LOADING", "YES")
                }
                ResponseWrapper.Status.SUCCESS -> {
                    it.body?.data?.products?.let { productList ->
                        cartSummary = if (productList.isEmpty()) {
                            CartSummary()
                        } else {
                            it.body.data.summary
                        }
                        cartAdapter.setItemList(productList, false)
                        if (diningReservationId == null)
                            cartAdapter.addFooter(CartProduct(CartProductItem(name = resources.getString(R.string.reserve))))
                        else
                            cartAdapter.addFooter(CartProduct(CartProductItem(name = resources.getString(R.string.order))))
                        cartAdapter.notifyDataSetChanged()
                        iLoadingCommunicator.updateLoadingProgress()
                    }
                }
                else -> {
                    with(iLoadingCommunicator){
                        updateLoadingProgress()
                        if (getProgress().first == getProgress().second){
                            iLoadingCommunicator.onError(it.body?.errors.toString(), retryCallbackList)
                        }
                    }
                }
            }
        })
        cartViewModel.addOrderToReservation.observe(viewLifecycleOwner, { event ->
            event.getContentIfNotHandled()?.let {
                when(it.status){
                    ResponseWrapper.Status.SUCCESS -> {
                        iLoadingCommunicator.updateLoadingProgress()
                        Toast.makeText(context, "ORDER ADDED TO RESERVATION", Toast.LENGTH_SHORT).show()
                    }
                    else -> {
                        Toast.makeText(context, "ERROR : "+it.body?.errors, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        })
    }

    private fun initRecyclerView(){
        with(viewBinding.rvCartProductList){
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = cartAdapter.apply {
                addItemDecoration(SpacingItemDecoration(bottomMargin = 20))
            }
        }
    }

    private fun onCartItemChanged(cartProduct: CartProduct, quantity: Int){
        val cartRequest = CartRequest(cartProduct.product?.productId, quantity)
        iLoadingCommunicator.startLoading(1, LoadingMode.ONLY_PROGRESS_BAR)
        retryCallbackList.removeAll { it.first == 1 }
        retryCallbackList.add(Pair(1, object : IRetryCallBack{
            override fun retry() {
                cartViewModel.callUpdateCurrentCartApi(cartRequest)
            }
        }))
        cartViewModel.callUpdateCurrentCartApi(cartRequest)
    }

    private fun onReserveButtonClicked(){
        diningReservationId?.let {
            iLoadingCommunicator.startLoading(1, LoadingMode.ONLY_PROGRESS_BAR)
            cartViewModel.addOrderToReservation(it)
        } ?: run {
            findNavController().navigate(CartFragmentDirections.reservationStep1())
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _viewBinding = null
    }

    override fun getCartSummary(): CartSummary? {
        return cartSummary
    }

}