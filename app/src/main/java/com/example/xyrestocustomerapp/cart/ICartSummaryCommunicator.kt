package com.example.xyrestocustomerapp.cart

import com.example.xyrestocustomerapp.cart.model.CartSummary

interface ICartSummaryCommunicator {
    fun getCartSummary(): CartSummary?
}