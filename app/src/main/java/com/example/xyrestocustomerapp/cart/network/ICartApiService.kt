package com.example.xyrestocustomerapp.cart.network

import com.example.xyrestocustomerapp.base.model.BaseApiResponse
import com.example.xyrestocustomerapp.base.model.Product
import com.example.xyrestocustomerapp.cart.model.CartRequest
import com.example.xyrestocustomerapp.cart.model.CartResponse
import com.example.xyrestocustomerapp.home.view.HomeFragment
import com.example.xyrestocustomerapp.reservation.model.ReservationHistoryResponse
import com.example.xyrestocustomerapp.reservation.model.ReservationItem
import com.example.xyrestocustomerapp.reservation.model.ReservationRequest
import com.example.xyrestocustomerapp.reservation.model.ReservationResponse
import retrofit2.Response
import retrofit2.http.*

interface ICartApiService {
    @Headers("Content-Type:application/json")
    @PUT("cart/current-cart")
    suspend fun updateCurrentCart(
        @Body cartRequest: CartRequest? = null
    ): Response<BaseApiResponse<CartResponse>>

    @GET("cart/current-cart")
    suspend fun getCurrentCart(): Response<BaseApiResponse<CartResponse>>

    @GET("reservation/reservations")
    suspend fun getAllReservation(): Response<BaseApiResponse<List<ReservationResponse>>>

    @PUT("reservation/reservations/{reservationId}/orders")
    suspend fun addOrderToReservation(@Path("reservationId") id: Int, @Body reservationOrder: List<CartRequest>): Response<BaseApiResponse<ReservationResponse>>
}