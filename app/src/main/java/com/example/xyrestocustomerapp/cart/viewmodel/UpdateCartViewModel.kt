package com.example.xyrestocustomerapp.cart.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.xyrestocustomerapp.base.model.BaseApiResponse
import com.example.xyrestocustomerapp.base.model.Event
import com.example.xyrestocustomerapp.base.utils.ApiCallHelper.safeApiCall
import com.example.xyrestocustomerapp.base.utils.ResponseWrapper
import com.example.xyrestocustomerapp.cart.model.CartRequest
import com.example.xyrestocustomerapp.cart.model.CartResponse
import com.example.xyrestocustomerapp.cart.repository.CartRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class UpdateCartViewModel(private val repository: CartRepository): ViewModel() {
    private val _cartResponse = MutableLiveData<Event<ResponseWrapper<BaseApiResponse<CartResponse>>>>()
    val cartResponse: LiveData<Event<ResponseWrapper<BaseApiResponse<CartResponse>>>> get() = _cartResponse

    fun callUpdateCurrentCartApi(cartRequest: CartRequest)  {
        viewModelScope.launch(Dispatchers.IO) {
            val prevCartResponse = safeApiCall { repository.callGetCurrentCartApi() }
            if (prevCartResponse.status == ResponseWrapper.Status.SUCCESS){
                val prevProduct =
                    prevCartResponse.body?.data?.products?.firstOrNull { it.product?.productId == cartRequest.productId }
                if (prevProduct == null)
                    _cartResponse.postValue(Event(safeApiCall { repository.callUpdateCartApi(cartRequest)}) )
                else
                    _cartResponse.postValue(Event(safeApiCall { repository.callUpdateCartApi(CartRequest(cartRequest.productId, prevProduct.quantity?.plus(1))) }))
            } else {
                _cartResponse.postValue(Event(prevCartResponse))
            }
        }
    }
}