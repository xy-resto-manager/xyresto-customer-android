package com.example.xyrestocustomerapp.cart.repository

import com.example.xyrestocustomerapp.cart.model.CartRequest
import com.example.xyrestocustomerapp.cart.network.ICartApiService

class CartRepository(private val iCartApiService: ICartApiService) {
    suspend fun callUpdateCartApi(cartRequest: CartRequest) = iCartApiService.updateCurrentCart(cartRequest = cartRequest)
    suspend fun callGetCurrentCartApi() = iCartApiService.getCurrentCart()
    suspend fun getAllReservation() = iCartApiService.getAllReservation()
    suspend fun addOrderToReservation(id: Int, reservationOrder: List<CartRequest>) = iCartApiService.addOrderToReservation(id, reservationOrder)
}