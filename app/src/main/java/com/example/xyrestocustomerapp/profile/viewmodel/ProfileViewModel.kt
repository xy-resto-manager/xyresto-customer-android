package com.example.xyrestocustomerapp.profile.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.xyrestocustomerapp.base.model.BaseApiResponse
import com.example.xyrestocustomerapp.base.model.User
import com.example.xyrestocustomerapp.base.utils.ApiCallHelper.safeApiCall
import com.example.xyrestocustomerapp.base.utils.ResponseWrapper
import com.example.xyrestocustomerapp.profile.model.PasswordRequest
import com.example.xyrestocustomerapp.profile.repository.ProfileRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ProfileViewModel(private val repository: ProfileRepository): ViewModel() {
    private val _userReponse = MutableLiveData<ResponseWrapper<BaseApiResponse<User>>>()
    val userResponse: LiveData<ResponseWrapper<BaseApiResponse<User>>>
        get() = _userReponse
    private val _passwordChangeReponse = MutableLiveData<ResponseWrapper<BaseApiResponse<Any>>>()
    val passwordChangeResponse: LiveData<ResponseWrapper<BaseApiResponse<Any>>>
        get() = _passwordChangeReponse
    private val _logoutResponse = MutableLiveData<ResponseWrapper<BaseApiResponse<Any>>>()
    val logoutResponse: LiveData<ResponseWrapper<BaseApiResponse<Any>>>
        get() = _logoutResponse

    init {
        getCurrentUser()
    }

    fun getCurrentUser(){
        viewModelScope.launch(Dispatchers.IO) {
            _userReponse.postValue(ResponseWrapper.loading())
            val result = safeApiCall { repository.getCurrentUser() }
            _userReponse.postValue(result)
        }
    }

    fun updatePassword(passwordRequest: PasswordRequest){
        viewModelScope.launch(Dispatchers.IO) {
            _passwordChangeReponse.postValue(ResponseWrapper.loading())
            val result = safeApiCall { repository.updatePassword(passwordRequest) }
            _passwordChangeReponse.postValue(result)
        }
    }

    fun logout(){
        viewModelScope.launch(Dispatchers.IO) {
            _logoutResponse.postValue(ResponseWrapper.loading())
            val result = safeApiCall { repository.logout() }
            _logoutResponse.postValue(result)
        }
    }
}