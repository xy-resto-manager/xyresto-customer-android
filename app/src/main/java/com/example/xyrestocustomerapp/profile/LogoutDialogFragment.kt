package com.example.xyrestocustomerapp.profile

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import com.example.xyrestocustomerapp.R
import com.example.xyrestocustomerapp.databinding.FragmentLogoutDialogBinding

class LogoutDialogFragment : DialogFragment() {

    private var _viewBinding: FragmentLogoutDialogBinding? = null
    private val viewBinding: FragmentLogoutDialogBinding get() = _viewBinding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _viewBinding = FragmentLogoutDialogBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView(){
        with(viewBinding){
            ivClose.setOnClickListener { dismiss() }
            btnNo.setOnClickListener { dismiss() }
            btnYes.setOnClickListener {
                val fragmentCommunicator: IProfileFragmentCommunicator = parentFragment as ProfileFragment
                fragmentCommunicator.logoutBtnClicked()
                dismiss()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _viewBinding = null
    }
}