package com.example.xyrestocustomerapp.profile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.widget.Toast
import androidx.activity.viewModels
import com.example.xyrestocustomerapp.R
import com.example.xyrestocustomerapp.base.storage.network.ApiClient
import com.example.xyrestocustomerapp.base.utils.ResponseWrapper
import com.example.xyrestocustomerapp.databinding.ActivityChangePasswordBinding
import com.example.xyrestocustomerapp.profile.model.PasswordRequest
import com.example.xyrestocustomerapp.profile.network.IUserService
import com.example.xyrestocustomerapp.profile.repository.ProfileRepository
import com.example.xyrestocustomerapp.profile.viewmodel.ProfileViewModel
import com.example.xyrestocustomerapp.profile.viewmodel.ProfileViewModelFactory

class ChangePasswordActivity : AppCompatActivity() {
    private lateinit var viewBinding: ActivityChangePasswordBinding
    private val viewModel: ProfileViewModel by viewModels{
        ProfileViewModelFactory(ProfileRepository(ApiClient.getRetrofitInstance(this).create(IUserService::class.java)))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityChangePasswordBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)
        initView()
        initObserver()
    }

    private fun initView(){
        with(viewBinding){
            layoutActionBar.toolbar.title = ""
            setSupportActionBar(layoutActionBar.toolbar)
            layoutActionBar.tvTitle.text = resources.getString(R.string.user_settings)
            etOldPass.transformationMethod = PasswordTransformationMethod.getInstance()
            etNewPass.transformationMethod = PasswordTransformationMethod.getInstance()
            etConfirmPass.transformationMethod = PasswordTransformationMethod.getInstance()
            layoutActionBar.ivBtnBack.setOnClickListener { onBackPressed() }
            btnSubmit.setOnClickListener { updatePassword() }
        }
    }

    private fun initObserver(){
        viewModel.passwordChangeResponse.observe(this, {
            when(it.status){
                ResponseWrapper.Status.LOADING -> {

                }
                ResponseWrapper.Status.SUCCESS -> {
                    Toast.makeText(this, resources.getString(R.string.update_password_success), Toast.LENGTH_SHORT).show()
                }
                else -> {
                    Toast.makeText(this, "ERROR: "+it.body?.errors, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun updatePassword(){
        val oldPassword = viewBinding.etOldPass.text.toString()
        val newPassword = viewBinding.etNewPass.text.toString()
        val confirmNewPassword = viewBinding.etConfirmPass.text.toString()
        if (newPassword != confirmNewPassword){
            Toast.makeText(this, resources.getString(R.string.password_not_matched), Toast.LENGTH_SHORT).show()
        } else {
            val passwordRequest = PasswordRequest(oldPassword, newPassword)
            viewModel.updatePassword(passwordRequest)
        }
    }
}