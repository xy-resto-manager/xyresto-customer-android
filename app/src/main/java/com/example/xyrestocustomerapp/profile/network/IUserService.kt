package com.example.xyrestocustomerapp.profile.network

import com.example.xyrestocustomerapp.base.model.BaseApiResponse
import com.example.xyrestocustomerapp.base.model.User
import com.example.xyrestocustomerapp.profile.model.PasswordRequest
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST

interface IUserService {
    @GET("user/current-user")
    suspend fun getCurrentUser(): Response<BaseApiResponse<User>>

    @POST("user/_change-password")
    suspend fun updatePassword(@Body passwordRequest: PasswordRequest): Response<BaseApiResponse<Any>>

    @POST("user/_logout")
    suspend fun logout(): Response<BaseApiResponse<Any>>
}