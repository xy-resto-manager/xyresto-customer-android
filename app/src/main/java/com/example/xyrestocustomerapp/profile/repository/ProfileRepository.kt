package com.example.xyrestocustomerapp.profile.repository

import com.example.xyrestocustomerapp.profile.model.PasswordRequest
import com.example.xyrestocustomerapp.profile.network.IUserService

class ProfileRepository(private val iUserService: IUserService) {
    suspend fun getCurrentUser() = iUserService.getCurrentUser()
    suspend fun updatePassword(passwordRequest: PasswordRequest)= iUserService.updatePassword(passwordRequest)
    suspend fun logout() = iUserService.logout()
}