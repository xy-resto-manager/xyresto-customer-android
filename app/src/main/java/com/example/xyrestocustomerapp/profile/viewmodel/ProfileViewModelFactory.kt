package com.example.xyrestocustomerapp.profile.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.xyrestocustomerapp.profile.repository.ProfileRepository

class ProfileViewModelFactory(private val profileRepository: ProfileRepository): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ProfileViewModel(profileRepository) as T
    }
}