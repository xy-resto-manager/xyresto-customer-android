package com.example.xyrestocustomerapp.profile

interface IProfileFragmentCommunicator {
    fun logoutBtnClicked()
}