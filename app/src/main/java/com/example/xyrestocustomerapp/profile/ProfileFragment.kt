package com.example.xyrestocustomerapp.profile

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.xyrestocustomerapp.base.storage.network.ApiClient
import com.example.xyrestocustomerapp.base.storage.session.BaseSharedPreference.Companion.USER_TOKEN
import com.example.xyrestocustomerapp.base.utils.ResponseWrapper
import com.example.xyrestocustomerapp.databinding.FragmentProfileBinding
import com.example.xyrestocustomerapp.home.IRetryCallBack
import com.example.xyrestocustomerapp.home.LoadingCommunicator
import com.example.xyrestocustomerapp.home.LoadingMode
import com.example.xyrestocustomerapp.home.view.HomeActivity
import com.example.xyrestocustomerapp.home.view.IHomeActivityCommunicator
import com.example.xyrestocustomerapp.profile.network.IUserService
import com.example.xyrestocustomerapp.profile.repository.ProfileRepository
import com.example.xyrestocustomerapp.profile.viewmodel.ProfileViewModel
import com.example.xyrestocustomerapp.profile.viewmodel.ProfileViewModelFactory

class ProfileFragment : Fragment(), IProfileFragmentCommunicator {

    private var _viewBinding: FragmentProfileBinding? = null
    private val viewBinding: FragmentProfileBinding get() = _viewBinding!!
    private val viewModel: ProfileViewModel by viewModels{
        ProfileViewModelFactory(ProfileRepository(ApiClient.getRetrofitInstance(requireContext()).create(IUserService::class.java)))
    }
    private lateinit var iActivityCommunicator: IHomeActivityCommunicator
    private val iLoadingCommunicator: LoadingCommunicator by lazy { activity as LoadingCommunicator }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _viewBinding = FragmentProfileBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initObserver()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        iActivityCommunicator = activity as HomeActivity
    }

    private fun initView(){
        with(viewBinding){
            etChangePass.setOnClickListener {
                val intent = Intent(context, ChangePasswordActivity::class.java)
                startActivity(intent)
            }
            etLogout.setOnClickListener {
                childFragmentManager.beginTransaction().add(LogoutDialogFragment(), null).commit()
            }
        }
    }

    private fun initObserver(){
        iLoadingCommunicator.startLoading(1, LoadingMode.PROGRESS_BAR_WITH_BACKGROUND)
        viewModel.userResponse.observe(viewLifecycleOwner, {
            when(it.status){
                ResponseWrapper.Status.LOADING -> {

                }
                ResponseWrapper.Status.SUCCESS -> {
                    viewBinding.tvUsername.text = it.body?.data?.fullName
                    iLoadingCommunicator.stopLoading()
                }
                else -> {
                    iLoadingCommunicator.onError(it.body?.errors.toString(), listOf(Pair(0, object : IRetryCallBack{
                        override fun retry() {
                            viewModel.getCurrentUser()
                        }
                    })))
                }
            }
        })
        viewModel.logoutResponse.observe(viewLifecycleOwner, {
            when(it.status){
                ResponseWrapper.Status.LOADING -> {

                }
                ResponseWrapper.Status.SUCCESS -> {
                    Toast.makeText(activity, "Logout Successfull", Toast.LENGTH_SHORT)
                    (activity as IHomeActivityCommunicator).getSessionManager().removeData(USER_TOKEN)
                    findNavController().navigate(ProfileFragmentDirections.home())
                }
                else -> {
                    Toast.makeText(context, "Logout Failed", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _viewBinding = null
    }

    override fun logoutBtnClicked() {
        viewModel.logout()
    }
}