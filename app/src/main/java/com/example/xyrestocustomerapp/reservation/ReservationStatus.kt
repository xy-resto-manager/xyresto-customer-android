package com.example.xyrestocustomerapp.reservation

object ReservationStatus {
    const val PENDING = "PENDING"
    const val APPROVED = "APPROVED"
    const val DECLINED = "DECLINED"
    const val DINING = "DINING"
    const val FINISHED = "FINISHED"
    const val CANCELED = "CANCELED"
}