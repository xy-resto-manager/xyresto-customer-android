package com.example.xyrestocustomerapp.reservation.step2.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners
import com.example.xyrestocustomerapp.R
import com.example.xyrestocustomerapp.base.adapter.BaseRvAdapter
import com.example.xyrestocustomerapp.base.constant.Constant.SERVER_URL
import com.example.xyrestocustomerapp.base.model.DateHolder
import com.example.xyrestocustomerapp.base.utils.Utils
import com.example.xyrestocustomerapp.base.utils.Utils.formatTime
import com.example.xyrestocustomerapp.cart.model.CartProduct
import com.example.xyrestocustomerapp.databinding.CartProductItemBinding
import com.example.xyrestocustomerapp.databinding.Reservation2FooterItemBinding
import com.example.xyrestocustomerapp.databinding.Reservation2HeaderItemBinding

class Reservation2Adapter(
    private val onProductChangeClicked: (CartProduct, Int) -> Unit,
    private val onReserveButtonClicked: () -> Unit,
    private val tableId: Int,
    private val reservationDate: DateHolder
): BaseRvAdapter<CartProduct>() {

    companion object {
        private const val VIEW_TYPE_NORMAL = 0
        private const val VIEW_TYPE_HEADER = 1
        private const val VIEW_TYPE_FOOTER = 2
    }

    private var totalPrice: Int? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseRvViewHolder<CartProduct> {
        return when(viewType) {
            VIEW_TYPE_NORMAL -> {
                val cartItemBinding = CartProductItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                CartViewHolder(cartItemBinding, onProductChangeClicked)
            }
            VIEW_TYPE_HEADER -> {
                val headerItemBinding = Reservation2HeaderItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                HeaderItemHolder(headerItemBinding)
            }
            else -> {
                val footerItemBinding = Reservation2FooterItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                FooterItemHolder(footerItemBinding, onReserveButtonClicked)
            }
        }
    }

    override fun onBindViewHolder(holder: BaseRvViewHolder<CartProduct>, position: Int) {
        mutableItems[position % mutableItemCount].let(holder::bind)
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) {
            VIEW_TYPE_HEADER
        } else if (position == mutableItemCount-1) {
            VIEW_TYPE_FOOTER
        } else {
            VIEW_TYPE_NORMAL
        }
    }

    fun setTotalPrice(totalPrice: Int){
        this.totalPrice = totalPrice
    }

    fun addFooterItem(value: CartProduct){
        mutableItems.add(value)
        notifyItemInserted(mutableItemCount - 1)
    }

    fun addHeaderItem(value: CartProduct){
        mutableItems.add(0, value)
        notifyItemInserted(0)
    }

    inner class CartViewHolder(
        private val binding: CartProductItemBinding,
        private val onProductChangeClicked: (CartProduct, Int) -> Unit
    ): BaseRvViewHolder<CartProduct>(binding.root){
        override fun bind(data: CartProduct, position: Int?) {
            with(binding){
                ivProduct.layout(0,0,0,0)
                Glide.with(root.context)
                    .load(SERVER_URL+data.product?.imageUrl)
                    .transform(CenterCrop(), GranularRoundedCorners(20.0F, 0.0F, 0.0F, 20.0F))
                    .placeholder(R.drawable.ic_food)
                    .into(ivProduct)
                tvProductTitle.text = data.product?.name
                tvProductPrice.text = data.product?.price.toString()
                tvProductQty.text = data.quantity.toString()

                ivBtnMinus.setOnClickListener {
                    val currentCount = Integer.parseInt(tvProductQty.text.toString()) - 1
                    onProductChangeClicked(data, currentCount)
                }

                ivBtnAdd.setOnClickListener {
                    val currentCount = Integer.parseInt(tvProductQty.text.toString()) + 1
                    onProductChangeClicked(data, currentCount)
                    Log.d("CURRENT COUNT", currentCount.toString())
                }

                ivBtnClose.setOnClickListener {
                    onProductChangeClicked(data, 0)
                }

            }
        }
    }

    inner class HeaderItemHolder(
        private val binding: Reservation2HeaderItemBinding
    ): BaseRvViewHolder<CartProduct>(binding.root){
        override fun bind(data: CartProduct, position: Int?) {
            with(binding){
                val context = root.context
                val date = reservationDate
                val twoHourAfter = (date.hourOfDay+2) % 24
                val time = "${date.hourOfDay.formatTime()}:${date.minute.formatTime()}-${twoHourAfter.formatTime()}:${date.minute.formatTime()}"
                tvDate.text = context.getString(R.string.reservation_date, Utils.formatDate(date.year, date.month-1, date.dayOfMonth))
                tvTime.text = context.getString(R.string.reservation_time, time)
                tvTableNumber.text = context.getString(R.string.table_number, tableId)
            }
        }
    }

    inner class FooterItemHolder(
        private val binding: Reservation2FooterItemBinding,
        private val onReserveButtonClicked: () -> Unit
    ): BaseRvViewHolder<CartProduct>(binding.root){
        override fun bind(data: CartProduct, position: Int?) {
            with(binding){
                val context = root.context
                tvTotalCost.text = context.getString(R.string.total_cost, totalPrice)
                btnConfirmReservation.setOnClickListener{
                    onReserveButtonClicked()
                }
            }
        }
    }
}