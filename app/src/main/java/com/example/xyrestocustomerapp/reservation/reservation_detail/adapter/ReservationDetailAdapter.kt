package com.example.xyrestocustomerapp.reservation.reservation_detail.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners
import com.example.xyrestocustomerapp.R
import com.example.xyrestocustomerapp.base.adapter.BaseRvAdapter
import com.example.xyrestocustomerapp.base.constant.Constant.SERVER_URL
import com.example.xyrestocustomerapp.base.utils.Utils.formatRp
import com.example.xyrestocustomerapp.databinding.ReservationDetailFooterBinding
import com.example.xyrestocustomerapp.databinding.ReservationDetailItemBinding
import com.example.xyrestocustomerapp.reservation.ReservationStatus
import com.example.xyrestocustomerapp.reservation.model.ReservationDetailItem

class ReservationDetailAdapter(
    private val onCancelButtonClicked: () -> Unit
): BaseRvAdapter<ReservationDetailItem>() {

    companion object {
        private const val VIEW_TYPE_NORMAL = 0
        private const val VIEW_TYPE_OTHER = 1
    }

    private lateinit var reservationStatus: String

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseRvViewHolder<ReservationDetailItem> {
        return when(viewType) {
            VIEW_TYPE_NORMAL -> {
                val productItemBinding = ReservationDetailItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                ReservationDetailViewHolder(productItemBinding)
            }
            else -> {
                val footerItemBinding = ReservationDetailFooterBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                OtherTypeHolder(footerItemBinding)
            }
        }
    }

    override fun onBindViewHolder(holder: BaseRvViewHolder<ReservationDetailItem>, position: Int) {
        mutableItems[position % mutableItemCount].let(holder::bind)
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == mutableItemCount-1) {
            VIEW_TYPE_OTHER
        } else {
            VIEW_TYPE_NORMAL
        }
    }

    fun addInfoItem(value: ReservationDetailItem){
        mutableItems.add(value)
        notifyItemInserted(mutableItemCount - 1)
    }

    fun setReservationStatus(status: String){
        reservationStatus = status
    }

    inner class ReservationDetailViewHolder(
        private val binding: ReservationDetailItemBinding
    ): BaseRvViewHolder<ReservationDetailItem>(binding.root){
        override fun bind(data: ReservationDetailItem, position: Int?) {
            with(binding){
                ivProduct.layout(0,0,0,0)
                Glide.with(root.context)
                    .load(SERVER_URL+data.product?.imageUrl)
                    .transform(CenterCrop(), GranularRoundedCorners(20.0F, 0.0F, 0.0F, 20.0F))
                    .placeholder(R.drawable.ic_food)
                    .into(ivProduct)
                tvProductTitle.text = data.product?.name
                tvProductPrice.text = data.product?.price.formatRp()
                tvProductQty.text = root.context.getString(R.string.quantity, data.quantity)
            }
        }
    }

    inner class OtherTypeHolder(
        private val binding: ReservationDetailFooterBinding
    ): BaseRvViewHolder<ReservationDetailItem>(binding.root){
        override fun bind(data: ReservationDetailItem, position: Int?) {
            with(binding){
                if (::reservationStatus.isInitialized){
                    if (!reservationStatus.equals(ReservationStatus.PENDING, true) && !reservationStatus.equals(ReservationStatus.APPROVED, true)){
                        btnCancel.visibility = View.GONE
                        btnCancel.setOnClickListener(null)
                    } else {
                        btnCancel.visibility = View.VISIBLE
                        btnCancel.setOnClickListener{
                            onCancelButtonClicked()
                        }
                    }
                }
            }
        }
    }
}