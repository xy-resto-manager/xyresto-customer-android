package com.example.xyrestocustomerapp.reservation.reservation_history.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.example.xyrestocustomerapp.R
import com.example.xyrestocustomerapp.base.adapter.FooterAdapter
import com.example.xyrestocustomerapp.base.adapter.LoadingHolder
import com.example.xyrestocustomerapp.base.utils.Utils
import com.example.xyrestocustomerapp.base.utils.Utils.orDefaultInt
import com.example.xyrestocustomerapp.databinding.ReservationHistoryItemBinding
import com.example.xyrestocustomerapp.reservation.ReservationStatus
import com.example.xyrestocustomerapp.reservation.model.ReservationHistoryResponse

class ReservationHistoryAdapter(
    private val onReservationDetailClicked: (ReservationHistoryResponse, Int) -> Unit
): FooterAdapter<ReservationHistoryResponse>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseRvViewHolder<ReservationHistoryResponse> {
        when(viewType) {
            VIEW_TYPE_CONTENT -> {
                val reservationItemBinding = ReservationHistoryItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                return ReservationHistoryViewHolder(reservationItemBinding, onReservationDetailClicked)
            }
            else -> {
                LayoutInflater.from(parent.context).inflate(R.layout.loading_item, parent, false)
                    .let {
                        return LoadingHolder(it)
                    }
            }
        }
    }

    override fun onBindViewHolder(holder: BaseRvViewHolder<ReservationHistoryResponse>, position: Int) {
        mutableItems[position % mutableItemCount].let(holder::bind)
    }

    override fun getItemViewType(position: Int): Int {
        return if (isFooterVisible && position == mutableItemCount-1) {
            VIEW_TYPE_FOOTER
        } else {
            VIEW_TYPE_CONTENT
        }
    }

    inner class ReservationHistoryViewHolder(
        private val binding: ReservationHistoryItemBinding,
        private val onReservationDetailClicked: (ReservationHistoryResponse, Int) -> Unit
    ): BaseRvViewHolder<ReservationHistoryResponse>(binding.root){
        override fun bind(data: ReservationHistoryResponse, position: Int?) {
            with(binding){
                val context = root.context
                tvDate.text = Utils.getSpannableBold(
                    context.resources.getString(R.string.reservation_date, Utils.timeStampToDate(data.timestamp!!, "dd MMMM yyyy")),
                    0,
                    context.resources.getInteger(R.integer.reservation_date_span_end)
                )
                val reservationTime = context.resources.getString(R.string.reservation_time, Utils.timeStampToDate(data.timestamp, "HH:mm")+"-"+Utils.timeStampToDate(data.timestamp+7200000, "HH:mm"))
                tvTime.text = Utils.getSpannableBold(
                    reservationTime,
                    0,
                    context.resources.getInteger(R.integer.reservation_date_span_end)
                )
                tvTableNumber.text = Utils.getSpannableBold(
                    context.resources.getString(R.string.table_number, data.tableId),
                    0,
                    context.resources.getInteger(R.integer.reservation_table_span_end)
                )
                when(data.status){
                    ReservationStatus.PENDING -> {
                        ivReservationStatus.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_reservation_status_awaiting))
                    }
                    ReservationStatus.APPROVED -> {
                        ivReservationStatus.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_reservation_status_approved))
                    }
                    ReservationStatus.DECLINED -> {
                        ivReservationStatus.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_reservation_status_declined))
                    }
                    ReservationStatus.DINING -> {
                        ivReservationStatus.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_reservation_status_dining))
                    }
                    ReservationStatus.FINISHED -> {
                        ivReservationStatus.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_reservation_status_finished))
                    }
                    ReservationStatus.CANCELED -> {
                        ivReservationStatus.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_reservation_status_cancelled))
                    }
                }
                btnViewDetail.setOnClickListener { onReservationDetailClicked(data, position.orDefaultInt()) }
            }
        }
    }
}