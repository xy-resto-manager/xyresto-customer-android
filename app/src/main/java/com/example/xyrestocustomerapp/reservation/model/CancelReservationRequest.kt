package com.example.xyrestocustomerapp.reservation.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CancelReservationRequest(
    @SerializedName("canceled")
    val canceled: Boolean = true
): Parcelable
