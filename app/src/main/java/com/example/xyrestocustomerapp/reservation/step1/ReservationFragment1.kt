package com.example.xyrestocustomerapp.reservation.step1

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.xyrestocustomerapp.R
import com.example.xyrestocustomerapp.base.model.DateHolder
import com.example.xyrestocustomerapp.base.storage.network.ApiClient
import com.example.xyrestocustomerapp.base.utils.ResponseWrapper
import com.example.xyrestocustomerapp.base.utils.Utils
import com.example.xyrestocustomerapp.base.utils.Utils.formatTime
import com.example.xyrestocustomerapp.base.utils.Utils.orDefaultBool
import com.example.xyrestocustomerapp.cart.network.ICartApiService
import com.example.xyrestocustomerapp.databinding.FragmentReservation1Binding
import com.example.xyrestocustomerapp.home.IRetryCallBack
import com.example.xyrestocustomerapp.home.LoadingCommunicator
import com.example.xyrestocustomerapp.home.LoadingMode
import com.example.xyrestocustomerapp.home.view.HomeActivity
import com.example.xyrestocustomerapp.home.view.IHomeActivityCommunicator
import com.example.xyrestocustomerapp.reservation.BaseTableFragment
import com.example.xyrestocustomerapp.reservation.model.TableEntity
import com.example.xyrestocustomerapp.reservation.network.IReservationService
import com.example.xyrestocustomerapp.reservation.repository.ReservationRepository
import com.example.xyrestocustomerapp.reservation.step1.viewmodel.Reservation1ViewModel
import com.example.xyrestocustomerapp.reservation.step1.viewmodel.Reservation1ViewModelFactory

private const val ARG_SELECTED_DATE = "selected_date"
private const val ARG_SELECTED_TABLE = "selected_table"

class ReservationFragment1 : BaseTableFragment() {
    private var _viewBinding: FragmentReservation1Binding? = null
    private val viewBinding get() = _viewBinding!!
    private val args: ReservationFragment1Args by navArgs()
    private val viewModel: Reservation1ViewModel by viewModels {
        Reservation1ViewModelFactory(
            ReservationRepository(
                ApiClient.getRetrofitInstance(requireContext()).create(IReservationService::class.java),
                ApiClient.getRetrofitInstance(requireContext()).create(ICartApiService::class.java)
            )
        )
    }
    private lateinit var iHomeActivityCommunicator: IHomeActivityCommunicator
    private lateinit var selectedDate: DateHolder
    private val iLoadingCommunicator: LoadingCommunicator by lazy { activity as LoadingCommunicator }
    private val retryCallbackList = mutableListOf<Pair<Int, IRetryCallBack>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.let {
            selectedTable = it.getParcelable(ARG_SELECTED_TABLE)
            it.getParcelable<DateHolder>(ARG_SELECTED_DATE)?.let { date ->
                selectedDate = date
            }
        } ?: run {
            initDate()
            viewModel.callTablesApi(getTimeStampSelectedDate())
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        iHomeActivityCommunicator = activity as HomeActivity
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _viewBinding = FragmentReservation1Binding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        gridLayout = viewBinding.gridTableLayout
        initView()
        initObserver()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(ARG_SELECTED_DATE, selectedDate)
        selectedTable?.let { outState.putParcelable(ARG_SELECTED_TABLE, it) }
    }

    private fun initView(){
        with(viewBinding){
            ArrayAdapter.createFromResource(
                requireContext(),
                R.array.reservation_time,
                android.R.layout.simple_spinner_dropdown_item
            ).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinner.adapter = adapter
            }
            selectedDate.run {
                etDatepicker.setText(Utils.formatDate(year, month-1, dayOfMonth))
                calendarView.setDate(getTimeStampSelectedDate(), true, true)
                val index = resources.getStringArray(R.array.reservation_time).indexOfFirst { it.split("-")[0].contains(hourOfDay.formatTime()) }
                spinner.setSelection(index)
            }
            etDatepicker.setOnClickListener {
                if (cvDatePicker.isVisible){
                    cvDatePicker.visibility = View.GONE
                } else {
                    cvDatePicker.visibility = View.VISIBLE
                }
            }
            spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (cvDatePicker.isVisible)
                        cvDatePicker.visibility = View.GONE
                    selectedDate.hourOfDay = getTimeFromArray(position).split("-")[0].split(":")[0].formatTime()
                    callTablesApi()
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                    //nothing to do
                }

            }
            calendarView.setOnDateChangeListener { view, year, month, dayOfMonth ->
                selectedDate.let {
                    it.year = year
                    it.month = month + 1
                    it.dayOfMonth = dayOfMonth
                }
                etDatepicker.setText(Utils.formatDate(year, month, dayOfMonth))
                cvDatePicker.visibility = View.GONE
                callTablesApi()
            }
            btnReserve.setOnClickListener {
                selectedTable?.let {
                    findNavController().navigate(ReservationFragment1Directions.reservationStep2(it.tableId, selectedDate, args.isFromCart))
                } ?: run {
                    Toast.makeText(context, resources.getString(R.string.you_must_select_table), Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun initObserver(){
        viewModel.tables.observe(viewLifecycleOwner, {
            retryCallbackList.removeAll { it.first == 0 }
            when(it.status){
                ResponseWrapper.Status.LOADING -> {

                }
                ResponseWrapper.Status.SUCCESS -> {
                    initGrid()
                    with(viewBinding.gridTableLayout) {
                        for (data in it.body?.data!!) {
                            (this[convertXY(data.table?.x!!-1,data.table.y!!-1)] as Button).run {
                                text = data.table.tableId.toString()
                                if (data.booked.orDefaultBool()) {
                                    isEnabled = false
                                    changeButtonBackground(this, 2)
                                } else {
                                    isEnabled = true
                                    if (selectedTable?.tableId == data.table.tableId){
                                        changeButtonBackground(this, 1)
                                    } else {
                                        changeButtonBackground(this, 0)
                                    }
                                    setOnClickListener {
                                        if (selectedTable?.tableId != data.table.tableId) {
                                            getSelectedTableView()?.let { prevBtn ->
                                                changeButtonBackground(prevBtn, 0)
                                            }
                                            selectedTable = data.run { TableEntity(table?.tableId!!, table.x!!-1, table.y!!-1, booked.orDefaultBool()) }
                                            changeButtonBackground(this, 1)
                                        } else {
                                            changeButtonBackground(this, 0)
                                            selectedTable = null
                                        }
                                    }
                                }
                            }
                        }
                    }
                    iLoadingCommunicator.updateLoadingProgress()
                }
                else -> {
                    with(iLoadingCommunicator){
                        updateLoadingProgress()
                        retryCallbackList.add(Pair(0, object : IRetryCallBack{
                            override fun retry() {
                                callTablesApi()
                            }
                        }))
                        if (getProgress().first == getProgress().second){
                            iLoadingCommunicator.onError(it.body?.errors.toString(), retryCallbackList)
                        }
                    }
                }
            }
        })
    }

    private fun initDate(){
        val dates = Utils.getSeparateDate()
        selectedDate = DateHolder().apply {
            dayOfMonth = dates[0]
            month = dates[1]
            year = dates[2]
            hourOfDay = getTimeFromArray(0).split("-")[0].split(":")[0].formatTime()
            minute = 0
        }
    }

    private fun getTimeFromArray(position: Int): String{
        return resources.getStringArray(R.array.reservation_time)[position]
    }

    private fun changeButtonBackground(button: Button, type: Int){
        with(button){
            when(type){
                0 -> {
                    background = ContextCompat.getDrawable(
                        context,
                        R.drawable.button_background_yellow_outlined
                    )
                }
                1 -> {
                    background = ContextCompat.getDrawable(
                        context,
                        R.drawable.button_background_yellow
                    )
                }
                else -> {
                    background = ContextCompat.getDrawable(
                        context,
                        R.drawable.button_background_grey
                    )
                }
            }
        }
    }

    private fun getTimeStampSelectedDate(): Long{
        val strDate: String = selectedDate.run {
            dayOfMonth.toString()+"-"+month+"-"+year+" "+hourOfDay.formatTime()+":"+minute.formatTime()
        }
        val timeStamp = Utils.getTimeStampFormat(strDate, "dd-MM-yyyy HH:mm")
        return timeStamp
    }
    private fun callTablesApi(){
        iLoadingCommunicator.startLoading(1, LoadingMode.ONLY_PROGRESS_BAR)
        viewModel.callTablesApi(getTimeStampSelectedDate())
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _viewBinding = null
    }

}