package com.example.xyrestocustomerapp.reservation.reservation_detail

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.xyrestocustomerapp.R
import com.example.xyrestocustomerapp.base.storage.network.ApiClient
import com.example.xyrestocustomerapp.base.utils.ResponseWrapper
import com.example.xyrestocustomerapp.base.utils.Utils
import com.example.xyrestocustomerapp.base.utils.Utils.formatTime
import com.example.xyrestocustomerapp.base.utils.Utils.rangeTime
import com.example.xyrestocustomerapp.base.widget.decoration.SpacingItemDecoration
import com.example.xyrestocustomerapp.cart.network.ICartApiService
import com.example.xyrestocustomerapp.databinding.FragmentReservationDetailBinding
import com.example.xyrestocustomerapp.home.IRetryCallBack
import com.example.xyrestocustomerapp.home.LoadingCommunicator
import com.example.xyrestocustomerapp.home.LoadingMode
import com.example.xyrestocustomerapp.reservation.ReservationStatus
import com.example.xyrestocustomerapp.reservation.model.CancelReservationRequest
import com.example.xyrestocustomerapp.reservation.model.ReservationDetailItem
import com.example.xyrestocustomerapp.reservation.model.ReservationDetailResponse
import com.example.xyrestocustomerapp.reservation.network.IReservationService
import com.example.xyrestocustomerapp.reservation.repository.ReservationRepository
import com.example.xyrestocustomerapp.reservation.reservation_detail.adapter.ReservationDetailAdapter
import com.example.xyrestocustomerapp.reservation.reservation_detail.viewmodel.ReservationDetailViewModel
import com.example.xyrestocustomerapp.reservation.reservation_detail.viewmodel.ReservationDetailViewModelFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext

class ReservationFragmentDetail : Fragment() {
    private var _viewBinding: FragmentReservationDetailBinding? = null
    private val viewBinding: FragmentReservationDetailBinding get() = _viewBinding!!
    private val reservationDetailAdapter = ReservationDetailAdapter(::onCancelReservationClicked)
    private val viewModel: ReservationDetailViewModel by viewModels {
        ReservationDetailViewModelFactory(
            ReservationRepository(
                ApiClient.getRetrofitInstance(requireContext()).create(IReservationService::class.java),
                ApiClient.getRetrofitInstance(requireContext()).create(ICartApiService::class.java)
            )
        )
    }
    private val args: ReservationFragmentDetailArgs by navArgs()
    private val iLoadingCommunicator: LoadingCommunicator by lazy { activity as LoadingCommunicator }
    private val retryCallbackList = mutableListOf<Pair<Int, IRetryCallBack>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _viewBinding = FragmentReservationDetailBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        initObserver()
        viewModel.getReservationDetail(args.reservationId)
    }

    private fun initRecyclerView(){
        with(viewBinding.rvProductList){
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = reservationDetailAdapter.apply {
                addItemDecoration(SpacingItemDecoration(bottomMargin = 20))
            }
        }
    }

    private fun initObserver(){
        iLoadingCommunicator.startLoading(1, LoadingMode.PROGRESS_BAR_WITH_BACKGROUND)
        viewModel.reservationDetail.observe(viewLifecycleOwner, {
            retryCallbackList.removeAll { it.first == 0 }
            when(it.status){
                ResponseWrapper.Status.LOADING -> {

                }
                ResponseWrapper.Status.SUCCESS -> {
                    it.body?.data?.status?.let { status -> reservationDetailAdapter.setReservationStatus(status) }
                    it.body?.data?.orders?.let { orders -> reservationDetailAdapter.setItemList(orders) }
                    reservationDetailAdapter.addInfoItem(ReservationDetailItem())
                    it.body?.data?.let { data -> setReservationDetail(data) }
                    iLoadingCommunicator.updateLoadingProgress()
                }
                else -> {
                    with(iLoadingCommunicator){
                        retryCallbackList.add(Pair(0, object : IRetryCallBack{
                            override fun retry() {
                                viewModel.getReservationDetail(args.reservationId)
                            }
                        }))
                        if (getProgress().first == getProgress().second){
                            iLoadingCommunicator.onError(it.body?.errors.toString(), retryCallbackList)
                        }
                    }
                }
            }
        })
        viewModel.cancelReservationResponse.observe(viewLifecycleOwner, {
            when(it.status){
                ResponseWrapper.Status.LOADING -> {

                }
                ResponseWrapper.Status.SUCCESS -> {
                    Toast.makeText(context, "Cancel Reservation Successfull", Toast.LENGTH_SHORT).show()
                    runBlocking { setDelay() }
                    findNavController().popBackStack()
                    iLoadingCommunicator.updateLoadingProgress()
                }
                else -> {
                    Log.d("ERROR CANCEL RESERV", it.body?.code.toString() +" "+ it.body?.status)
                }
            }
        })
    }

    private fun setReservationDetail(data: ReservationDetailResponse){
        with(viewBinding){
            tvBookingId.text = resources.getString(R.string.booking_id, data.id.toString())
            data.timestamp?.let {
                tvDate.text = resources.getString(R.string.reservation_date, Utils.timeStampToDate(it, "E, dd MMM yyy"))
                tvTime.text = resources.getString(R.string.reservation_time,rangeTime(it, 2))
            } ?: run {
                tvDate.text = resources.getString(R.string.reservation_date, "-")
                tvTime.text = resources.getString(R.string.reservation_time,"-")
            }
            data.tableId?.let { tvTableNumber.text = resources.getString(R.string.table_number, it) }
            if (data.orders.isNullOrEmpty()){
                tvListMenuTitle.visibility = View.GONE
            } else {
                tvListMenuTitle.visibility = View.VISIBLE
            }
            ivReservationStatus.run {
                when(data.status){
                    ReservationStatus.PENDING -> {
                        setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_reservation_status_awaiting))
                    }
                    ReservationStatus.APPROVED -> {
                        setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_reservation_status_approved))
                    }
                    ReservationStatus.DECLINED -> {
                        setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_reservation_status_declined))
                    }
                    ReservationStatus.DINING -> {
                        setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_reservation_status_dining))
                    }
                    ReservationStatus.FINISHED -> {
                        setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_reservation_status_finished))
                    }
                    ReservationStatus.CANCELED -> {
                        setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_reservation_status_cancelled))
                    }
                }
            }
        }
    }

    private fun onCancelReservationClicked(){
        iLoadingCommunicator.startLoading(1, LoadingMode.ONLY_PROGRESS_BAR)
        viewModel.cancelReservation(args.reservationId, CancelReservationRequest())
    }

    private suspend fun setDelay(timeMillis: Long = 1000){
        withContext(Dispatchers.IO){
            delay(timeMillis)
        }
    }
}