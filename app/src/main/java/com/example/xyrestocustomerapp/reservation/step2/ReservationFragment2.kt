package com.example.xyrestocustomerapp.reservation.step2

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.xyrestocustomerapp.R
import com.example.xyrestocustomerapp.base.storage.network.ApiClient
import com.example.xyrestocustomerapp.base.utils.ResponseWrapper
import com.example.xyrestocustomerapp.base.utils.Utils
import com.example.xyrestocustomerapp.base.utils.Utils.formatTime
import com.example.xyrestocustomerapp.base.widget.decoration.SpacingItemDecoration
import com.example.xyrestocustomerapp.cart.model.CartProduct
import com.example.xyrestocustomerapp.cart.model.CartRequest
import com.example.xyrestocustomerapp.cart.model.CartSummary
import com.example.xyrestocustomerapp.cart.network.ICartApiService
import com.example.xyrestocustomerapp.databinding.FragmentReservation2Binding
import com.example.xyrestocustomerapp.home.LoadingCommunicator
import com.example.xyrestocustomerapp.home.LoadingMode
import com.example.xyrestocustomerapp.home.view.HomeActivity
import com.example.xyrestocustomerapp.home.view.IHomeActivityCommunicator
import com.example.xyrestocustomerapp.reservation.model.Order
import com.example.xyrestocustomerapp.reservation.model.ReservationRequest
import com.example.xyrestocustomerapp.reservation.network.IReservationService
import com.example.xyrestocustomerapp.reservation.repository.ReservationRepository
import com.example.xyrestocustomerapp.reservation.step2.adapter.Reservation2Adapter
import com.example.xyrestocustomerapp.reservation.step2.viewmodel.Reservation2ViewModel
import com.example.xyrestocustomerapp.reservation.step2.viewmodel.Reservation2ViewModelFactory

class ReservationFragment2 : Fragment() {
    private var _viewBinding: FragmentReservation2Binding? = null
    private val viewBinding: FragmentReservation2Binding get() = _viewBinding!!
    private val args: ReservationFragment2Args by navArgs()
    private val reservationViewModel: Reservation2ViewModel by viewModels{
        Reservation2ViewModelFactory(
            ReservationRepository(
                ApiClient.getRetrofitInstance(requireContext()).create(IReservationService::class.java),
                ApiClient.getRetrofitInstance(requireContext()).create(ICartApiService::class.java)
            )
        )
    }
    private lateinit var iHomeActivityCommunicator: IHomeActivityCommunicator
    private val iLoadingCommunicator: LoadingCommunicator by lazy { activity as LoadingCommunicator }
    private val reservationAdapter by lazy {Reservation2Adapter(::onCartItemChanged, ::onReserveButtonClicked, args.tableId, args.reservationDate)}
    private val orderedProduct: MutableList<Order> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        iHomeActivityCommunicator = activity as IHomeActivityCommunicator
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _viewBinding = FragmentReservation2Binding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        initObserver()
        reservationViewModel.callGetCurrentCartApi()
    }

    private fun initRecyclerView(){
        with(viewBinding.rvProductList){
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = reservationAdapter.apply {
                addItemDecoration(SpacingItemDecoration(bottomMargin = 20))
            }
        }
    }

    private fun initObserver(){
        reservationViewModel.cartResponse.observe(viewLifecycleOwner, {
            when(it.first.status){
                ResponseWrapper.Status.LOADING -> {
                    Log.d("LOADING", "YES")
                }
                ResponseWrapper.Status.SUCCESS -> {
                    if (it.second == "UPDATE"){
                        Toast.makeText(context, "CART UPDATE SUCCESFULLY", Toast.LENGTH_SHORT).show()
                    }
                    it.first.body?.data?.products?.let { productList ->
                        it.first.body?.data?.summary?.let { summary ->
                            setCurrentCartItem(productList, summary)
                            createReservationRequest(productList)
                        }
                    }
                }
                else -> {
                    Toast.makeText(context, "ERROR: "+it.first.body?.errors, Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun createReservationRequest(cartPoductList: List<CartProduct>){
        orderedProduct.clear()
        for (data in cartPoductList){
            val order = Order(data.product?.productId, data.quantity)
            orderedProduct.add(order)
        }
    }

    private fun setCurrentCartItem(productList: List<CartProduct>, cartSummary: CartSummary){
        reservationAdapter.run {
            setTotalPrice(cartSummary.totalPrice)
            setItemList(productList)
            addHeaderItem(CartProduct())
            addFooterItem(CartProduct())
        }
    }

    private fun onCartItemChanged(cartProduct: CartProduct, quantity: Int){
        val cartRequest = CartRequest(cartProduct.product?.productId, quantity)
        reservationViewModel.callUpdateCurrentCartApi(cartRequest)
    }

    private fun onReserveButtonClicked(){
        iLoadingCommunicator.startLoading(1, LoadingMode.ONLY_PROGRESS_BAR)
        val strDate = args.reservationDate.run {
            dayOfMonth.toString()+"-"+month+"-"+year+" "+hourOfDay.formatTime()+":"+minute.formatTime()
        }
        val timestamp = Utils.getTimeStampFormat(strDate, "dd-MM-yyyy HH:mm")
        val reservationRequest = ReservationRequest(timestamp=timestamp, tableId = args.tableId, orders = orderedProduct)
        reservationViewModel.createReservation(reservationRequest).observe(viewLifecycleOwner, {
            when(it.status){
                ResponseWrapper.Status.LOADING -> {
                    Log.d("CREATE RESERVE", "LOADING")
                }
                ResponseWrapper.Status.SUCCESS -> {
                    findNavController().navigate(
                        ReservationFragment2Directions.reservationHistory(),
                        run {
                            if (args.isFromCart)
                                NavOptions.Builder().setPopUpTo(R.id.option_preorder, true).build()
                            else
                                NavOptions.Builder().setPopUpTo(R.id.option_reservation, true).build()
                        }
                    )
                    iLoadingCommunicator.updateLoadingProgress()
                }
                else -> {
                    Toast.makeText(context, "ERROR: "+it.body?.errors, Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _viewBinding = null
    }
}