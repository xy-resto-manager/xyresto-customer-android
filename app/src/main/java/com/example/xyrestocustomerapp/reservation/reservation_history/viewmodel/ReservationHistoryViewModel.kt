package com.example.xyrestocustomerapp.reservation.reservation_history.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.xyrestocustomerapp.base.model.BaseApiResponse
import com.example.xyrestocustomerapp.base.utils.ApiCallHelper.safeApiCall
import com.example.xyrestocustomerapp.base.utils.ResponseWrapper
import com.example.xyrestocustomerapp.reservation.model.ReservationHistoryResponse
import com.example.xyrestocustomerapp.reservation.repository.ReservationRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ReservationHistoryViewModel(private val repository: ReservationRepository): ViewModel() {
    private val _reservations = MutableLiveData<ResponseWrapper<BaseApiResponse<List<ReservationHistoryResponse>>>>()
    val reservations: LiveData<ResponseWrapper<BaseApiResponse<List<ReservationHistoryResponse>>>> get() = _reservations

    init {
        getReservations()
    }

    fun getReservations(){
        viewModelScope.launch(Dispatchers.IO) {
            _reservations.postValue(ResponseWrapper.loading())
            _reservations.postValue(safeApiCall { repository.getReservations() })
        }
    }
}