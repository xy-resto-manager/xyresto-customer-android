package com.example.xyrestocustomerapp.reservation.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class  TableEntity(
    @SerializedName("tableId")
    val tableId: Int = 0,
    @SerializedName("x")
    var x: Int = 0,
    @SerializedName("y")
    var y: Int = 0,
    var booked: Boolean = false
): Parcelable