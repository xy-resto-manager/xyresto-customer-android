package com.example.xyrestocustomerapp.reservation.network

import com.example.xyrestocustomerapp.base.model.BaseApiResponse
import com.example.xyrestocustomerapp.reservation.model.*
import retrofit2.Response
import retrofit2.http.*

interface IReservationService {
    @GET("layout/tables")
    suspend fun getTables(@Query("timestamp") timestamp: Long): Response<BaseApiResponse<List<TableResponse>>>

    @POST("reservation/reservations")
    suspend fun createReservation(@Body reservationRequest: ReservationRequest): Response<BaseApiResponse<ReservationResponse>>

    @GET("reservation/reservations")
    suspend fun getReservations(): Response<BaseApiResponse<List<ReservationHistoryResponse>>>

    @GET("reservation/reservations/{id}")
    suspend fun getReservationDetail(@Path("id") id: Int): Response<BaseApiResponse<ReservationDetailResponse>>

    @PUT("reservation/reservations/{id}")
    suspend fun cancelReservation(@Path("id") id: Int, @Body cancelReservationRequest: CancelReservationRequest): Response<BaseApiResponse<Any>>
}