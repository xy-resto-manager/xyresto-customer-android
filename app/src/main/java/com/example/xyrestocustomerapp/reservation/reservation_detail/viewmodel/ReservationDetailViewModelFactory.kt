package com.example.xyrestocustomerapp.reservation.reservation_detail.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.xyrestocustomerapp.reservation.repository.ReservationRepository

class ReservationDetailViewModelFactory(private val reservationRepository: ReservationRepository): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ReservationDetailViewModel(reservationRepository) as T
    }
}