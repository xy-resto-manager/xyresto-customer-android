package com.example.xyrestocustomerapp.reservation

import androidx.fragment.app.Fragment

interface IReservationFragmentCommunicator {
    fun loadFragment(fragment: Fragment, addToBackStack: Boolean = false, fragmentTag: String?)
    fun setActionBarTitle(title: String)
}