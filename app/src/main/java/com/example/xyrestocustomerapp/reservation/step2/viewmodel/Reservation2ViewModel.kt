package com.example.xyrestocustomerapp.reservation.step2.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.xyrestocustomerapp.base.model.BaseApiResponse
import com.example.xyrestocustomerapp.base.utils.ApiCallHelper
import com.example.xyrestocustomerapp.base.utils.ResponseWrapper
import com.example.xyrestocustomerapp.cart.model.CartRequest
import com.example.xyrestocustomerapp.cart.model.CartResponse
import com.example.xyrestocustomerapp.reservation.model.ReservationRequest
import com.example.xyrestocustomerapp.reservation.model.ReservationResponse
import com.example.xyrestocustomerapp.reservation.repository.ReservationRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class Reservation2ViewModel(private val repository: ReservationRepository): ViewModel() {
    private val _cartResponse = MutableLiveData<Pair<ResponseWrapper<BaseApiResponse<CartResponse>>, String>>()
    val cartResponse: LiveData<Pair<ResponseWrapper<BaseApiResponse<CartResponse>>, String>>
        get() = _cartResponse

    init {
        callGetCurrentCartApi()
    }

    fun callUpdateCurrentCartApi(cartRequest: CartRequest)  {
        viewModelScope.launch(Dispatchers.IO) {
            _cartResponse.postValue(Pair(ResponseWrapper.loading(), "UPDATE"))
            val result =
                ApiCallHelper.safeApiCall { repository.updateCurrentCart(cartRequest = cartRequest) }
            _cartResponse.postValue(Pair(result, "UPDATE"))
        }
    }

    fun callGetCurrentCartApi() {
        viewModelScope.launch(Dispatchers.IO) {
            _cartResponse.postValue(Pair(ResponseWrapper.loading(), "GET"))
            val result = ApiCallHelper.safeApiCall { repository.getCurrentCart() }
            _cartResponse.postValue(Pair(result, "GET"))
        }
    }

    fun createReservation(reservationRequest: ReservationRequest): LiveData<ResponseWrapper<BaseApiResponse<ReservationResponse>>>{
        val liveData = MutableLiveData<ResponseWrapper<BaseApiResponse<ReservationResponse>>>()
        viewModelScope.launch(Dispatchers.IO) {
            liveData.postValue(ResponseWrapper.loading())
            val result = ApiCallHelper.safeApiCall { repository.createReservation(reservationRequest) }
            liveData.postValue(result)
        }
        return liveData
    }
}