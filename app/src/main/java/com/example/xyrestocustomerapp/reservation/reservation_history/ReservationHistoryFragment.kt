package com.example.xyrestocustomerapp.reservation.reservation_history

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.xyrestocustomerapp.base.storage.network.ApiClient
import com.example.xyrestocustomerapp.base.utils.ResponseWrapper
import com.example.xyrestocustomerapp.base.widget.decoration.SpacingItemDecoration
import com.example.xyrestocustomerapp.cart.network.ICartApiService
import com.example.xyrestocustomerapp.databinding.FragmentReservationHistoryBinding
import com.example.xyrestocustomerapp.home.IRetryCallBack
import com.example.xyrestocustomerapp.home.LoadingCommunicator
import com.example.xyrestocustomerapp.home.LoadingMode
import com.example.xyrestocustomerapp.reservation.model.ReservationHistoryResponse
import com.example.xyrestocustomerapp.reservation.network.IReservationService
import com.example.xyrestocustomerapp.reservation.repository.ReservationRepository
import com.example.xyrestocustomerapp.reservation.reservation_history.adapter.ReservationHistoryAdapter
import com.example.xyrestocustomerapp.reservation.reservation_history.viewmodel.ReservationHistoryViewModel
import com.example.xyrestocustomerapp.reservation.reservation_history.viewmodel.ReservationHistoryViewModelFactory

class ReservationHistoryFragment : Fragment() {

    private var _viewBinding: FragmentReservationHistoryBinding? = null
    private val viewBinding: FragmentReservationHistoryBinding get() = _viewBinding!!
    private val viewModel: ReservationHistoryViewModel by viewModels {
        ReservationHistoryViewModelFactory(
            ReservationRepository(
                ApiClient.getRetrofitInstance(requireContext()).create(IReservationService::class.java),
                ApiClient.getRetrofitInstance(requireContext()).create(ICartApiService::class.java)
            )
        )
    }
    private val reservationHistoryAdapter by lazy { ReservationHistoryAdapter(::onReservationDetailClicked) }
    private val iLoadingCommunicator: LoadingCommunicator by lazy { activity as LoadingCommunicator }
    private val retryCallbackList = mutableListOf<Pair<Int, IRetryCallBack>>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _viewBinding = FragmentReservationHistoryBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewBinding.btnNewReservation.setOnClickListener {
            findNavController().navigate(ReservationHistoryFragmentDirections.reservationStep1())
        }
        initRecyclerView()
        initObserver()
    }

    private fun initRecyclerView(){
        with(viewBinding.rvReservationList){
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = reservationHistoryAdapter.apply {
                addItemDecoration(SpacingItemDecoration(topMargin = 40, leftMargin = 35, rightMargin = 35))
            }
        }
    }

    private fun initObserver(){
        iLoadingCommunicator.startLoading(1, LoadingMode.PROGRESS_BAR_WITH_BACKGROUND)
        viewModel.reservations.observe(viewLifecycleOwner, {
            retryCallbackList.removeAll { it.first == 0 }
            when(it.status){
                ResponseWrapper.Status.LOADING -> {
                    Log.d("LOADING", "YES")
                }
                ResponseWrapper.Status.SUCCESS -> {
                    if (it.body?.data.isNullOrEmpty()){
                      viewBinding.tvEmptyReservation.visibility = View.VISIBLE
                    } else {
                        it.body?.data?.let { reservationHistoryAdapter.setItemList(it) }
                    }
                    iLoadingCommunicator.updateLoadingProgress()
                }
                else -> {
                    with(iLoadingCommunicator){
                        updateLoadingProgress()
                        retryCallbackList.add(Pair(0, object : IRetryCallBack{
                            override fun retry() {
                                viewModel.getReservations()
                            }
                        }))
                        if (getProgress().first == getProgress().second){
                            iLoadingCommunicator.onError(it.body?.errors.toString(), retryCallbackList)
                        }
                    }
                }
            }
        })
    }

    private fun onReservationDetailClicked(data: ReservationHistoryResponse, position: Int){
        data.id?.let {
            findNavController().navigate(ReservationHistoryFragmentDirections.reservationDetail(it))
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _viewBinding = null
    }
}