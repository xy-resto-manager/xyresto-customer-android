package com.example.xyrestocustomerapp.reservation.repository

import com.example.xyrestocustomerapp.cart.model.CartRequest
import com.example.xyrestocustomerapp.cart.network.ICartApiService
import com.example.xyrestocustomerapp.reservation.model.CancelReservationRequest
import com.example.xyrestocustomerapp.reservation.model.ReservationRequest
import com.example.xyrestocustomerapp.reservation.network.IReservationService

class ReservationRepository(
    private val iReservationService: IReservationService,
    private val iCartService: ICartApiService
) {
    suspend fun callTablesApi(timestamp: Long) = iReservationService.getTables(timestamp)
    suspend fun createReservation(reservationRequest: ReservationRequest) = iReservationService.createReservation(reservationRequest)
    suspend fun getCurrentCart() = iCartService.getCurrentCart()
    suspend fun updateCurrentCart(cartRequest: CartRequest) = iCartService.updateCurrentCart(cartRequest)
    suspend fun getReservations() = iReservationService.getReservations()
    suspend fun getReservationDetail(id: Int) = iReservationService.getReservationDetail(id)
    suspend fun cancelReservation(id: Int, cancelReservationRequest: CancelReservationRequest) = iReservationService.cancelReservation(id, cancelReservationRequest)
}