package com.example.xyrestocustomerapp.reservation.reservation_detail.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.xyrestocustomerapp.base.model.BaseApiResponse
import com.example.xyrestocustomerapp.base.utils.ApiCallHelper.safeApiCall
import com.example.xyrestocustomerapp.base.utils.ResponseWrapper
import com.example.xyrestocustomerapp.reservation.model.CancelReservationRequest
import com.example.xyrestocustomerapp.reservation.model.ReservationDetailResponse
import com.example.xyrestocustomerapp.reservation.repository.ReservationRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ReservationDetailViewModel(private val repository: ReservationRepository): ViewModel() {
    private val _reservationDetail = MutableLiveData<ResponseWrapper<BaseApiResponse<ReservationDetailResponse>>>()
    val reservationDetail: LiveData<ResponseWrapper<BaseApiResponse<ReservationDetailResponse>>> get() = _reservationDetail
    private val _cancelReservationResponse = MutableLiveData<ResponseWrapper<BaseApiResponse<Any>>>()
    val cancelReservationResponse: LiveData<ResponseWrapper<BaseApiResponse<Any>>> get() = _cancelReservationResponse

    fun getReservationDetail(id: Int){
        viewModelScope.launch(Dispatchers.IO) {
            _reservationDetail.postValue(ResponseWrapper.loading())
            val result = safeApiCall { repository.getReservationDetail(id) }
            _reservationDetail.postValue(result)
        }
    }

    fun cancelReservation(id: Int, cancelReservationRequest: CancelReservationRequest){
        viewModelScope.launch {
            _cancelReservationResponse.postValue(ResponseWrapper.loading())
            val result = safeApiCall { repository.cancelReservation(id, cancelReservationRequest) }
            _cancelReservationResponse.postValue(result)
        }
    }
}