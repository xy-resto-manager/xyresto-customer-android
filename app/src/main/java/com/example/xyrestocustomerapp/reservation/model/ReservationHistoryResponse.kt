package com.example.xyrestocustomerapp.reservation.model

import android.os.Parcelable
import com.example.xyrestocustomerapp.base.model.User
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ReservationHistoryResponse(
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("timestamp")
    val timestamp: Long? = null,
    @SerializedName("user")
    val user: User? = null,
    @SerializedName("tableId")
    val tableId: Int? = null,
    @SerializedName("status")
    val status: String? = null
): Parcelable
