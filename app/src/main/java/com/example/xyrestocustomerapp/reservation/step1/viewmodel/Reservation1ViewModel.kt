package com.example.xyrestocustomerapp.reservation.step1.viewmodel

import androidx.lifecycle.*
import com.example.xyrestocustomerapp.base.model.BaseApiResponse
import com.example.xyrestocustomerapp.base.utils.ApiCallHelper.safeApiCall
import com.example.xyrestocustomerapp.base.utils.ResponseWrapper
import com.example.xyrestocustomerapp.reservation.model.TableResponse
import com.example.xyrestocustomerapp.reservation.repository.ReservationRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class Reservation1ViewModel(private val repository: ReservationRepository): ViewModel() {

    private val _tables = MutableLiveData<ResponseWrapper<BaseApiResponse<List<TableResponse>>>>()
    val tables: LiveData<ResponseWrapper<BaseApiResponse<List<TableResponse>>>> = _tables
    private var debouncePeriod: Long = 1000
    private var searchJob: Job? = null

    fun callTablesApi(timestamp: Long) {
        searchJob?.cancel()
        searchJob = viewModelScope.launch(Dispatchers.IO) {
            delay(debouncePeriod)
            val tables = safeApiCall{ repository.callTablesApi(timestamp) }
            _tables.postValue(tables)
        }
    }

    override fun onCleared() {
        super.onCleared()
        searchJob?.cancel()
    }
}