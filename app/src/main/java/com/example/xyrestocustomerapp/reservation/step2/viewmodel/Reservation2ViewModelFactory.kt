package com.example.xyrestocustomerapp.reservation.step2.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.xyrestocustomerapp.reservation.repository.ReservationRepository

class Reservation2ViewModelFactory(private val repository: ReservationRepository): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return Reservation2ViewModel(repository) as T
    }
}