package com.example.xyrestocustomerapp.reservation.step1.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.xyrestocustomerapp.reservation.repository.ReservationRepository

class Reservation1ViewModelFactory(private val reservationRepository: ReservationRepository): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return Reservation1ViewModel(reservationRepository) as T
    }
}