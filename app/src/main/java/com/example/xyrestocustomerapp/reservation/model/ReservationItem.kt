package com.example.xyrestocustomerapp.reservation.model

import android.os.Parcelable
import com.example.xyrestocustomerapp.base.model.Product
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ReservationItem(
    @SerializedName("product")
    val product: Product? = null,
    @SerializedName("quantity")
    val quantity: Int = 0,
    @SerializedName("served")
    val served: Boolean = false
): Parcelable