package com.example.xyrestocustomerapp.reservation.reservation_history.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.xyrestocustomerapp.reservation.repository.ReservationRepository

class ReservationHistoryViewModelFactory(private val reservationRepository: ReservationRepository): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ReservationHistoryViewModel(reservationRepository) as T
    }
}