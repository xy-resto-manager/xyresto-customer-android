package com.example.xyrestocustomerapp.register.network

import com.example.xyrestocustomerapp.base.model.BaseApiResponse
import com.example.xyrestocustomerapp.register.model.RegisterRequest
import com.example.xyrestocustomerapp.register.model.RegisterResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface IRegisterApiService {
    @POST("user/_register")
    suspend fun register(
        @Body registerRequest: RegisterRequest? = null
    ): Response<BaseApiResponse<RegisterResponse>>
}