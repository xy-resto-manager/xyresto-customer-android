package com.example.xyrestocustomerapp.register.repository

import com.example.xyrestocustomerapp.register.model.RegisterRequest
import com.example.xyrestocustomerapp.register.network.IRegisterApiService

class RegisterRepository(private val iRegisterApiService: IRegisterApiService) {
    suspend fun callRegisterApi(registerRequest: RegisterRequest) = iRegisterApiService.register(registerRequest)
}