package com.example.xyrestocustomerapp.register.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.xyrestocustomerapp.base.model.BaseApiResponse
import com.example.xyrestocustomerapp.base.utils.ApiCallHelper.safeApiCall
import com.example.xyrestocustomerapp.base.utils.ResponseWrapper
import com.example.xyrestocustomerapp.login.model.LoginRequest
import com.example.xyrestocustomerapp.login.model.LoginResponse
import com.example.xyrestocustomerapp.register.model.RegisterRequest
import com.example.xyrestocustomerapp.register.model.RegisterResponse
import com.example.xyrestocustomerapp.register.repository.RegisterRepository
import kotlinx.coroutines.launch

class RegisterViewModel(private val registerRepository: RegisterRepository): ViewModel() {
    private val _registerResponse = MutableLiveData<ResponseWrapper<BaseApiResponse<RegisterResponse>>>()
    val registerResponse: LiveData<ResponseWrapper<BaseApiResponse<RegisterResponse>>>
        get() = _registerResponse

    fun callRegisterApi(registerRequest: RegisterRequest) {
        viewModelScope.launch {
            _registerResponse.postValue(ResponseWrapper.loading())
            val result = safeApiCall { registerRepository.callRegisterApi(registerRequest) }
            _registerResponse.postValue(result)
        }
    }
}