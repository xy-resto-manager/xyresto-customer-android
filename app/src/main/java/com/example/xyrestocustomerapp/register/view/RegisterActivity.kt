package com.example.xyrestocustomerapp.register.view

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.method.PasswordTransformationMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import com.example.xyrestocustomerapp.R
import com.example.xyrestocustomerapp.base.constant.Constant
import com.example.xyrestocustomerapp.base.model.BaseApiResponse
import com.example.xyrestocustomerapp.base.storage.network.ApiClient
import com.example.xyrestocustomerapp.base.utils.ResponseWrapper
import com.example.xyrestocustomerapp.base.utils.Utils
import com.example.xyrestocustomerapp.databinding.ActivityRegisterBinding
import com.example.xyrestocustomerapp.home.view.HomeActivity
import com.example.xyrestocustomerapp.login.view.LoginActivity
import com.example.xyrestocustomerapp.register.model.RegisterRequest
import com.example.xyrestocustomerapp.register.model.RegisterResponse
import com.example.xyrestocustomerapp.register.network.IRegisterApiService
import com.example.xyrestocustomerapp.register.repository.RegisterRepository
import com.example.xyrestocustomerapp.register.viewmodel.RegisterViewModel
import com.example.xyrestocustomerapp.register.viewmodel.RegisterViewModelFactory

class RegisterActivity : AppCompatActivity() {

    private lateinit var viewBinding: ActivityRegisterBinding
    private val registerViewModel: RegisterViewModel by viewModels{
        RegisterViewModelFactory(RegisterRepository(ApiClient.getRetrofitInstance(this).create(IRegisterApiService::class.java)))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)
        initView()
        initObserver()
    }

    private fun initView(){
        with(viewBinding){
            layoutFieldName.tvLabel.text = resources.getString(R.string.label_name)
            layoutFieldEmail.tvLabel.text = resources.getString(R.string.label_email)
            layoutFieldPassword.tvLabel.text = resources.getString(R.string.label_password)
            layoutFieldConfirmPassword.tvLabel.text = resources.getString(R.string.label_confirm_password)
            layoutFieldPassword.etInput.transformationMethod = PasswordTransformationMethod.getInstance()
            layoutFieldConfirmPassword.etInput.transformationMethod = PasswordTransformationMethod.getInstance()

            btnRegister.setOnClickListener {
                callRegisterApi()
            }

            val spannableString = SpannableString(resources.getString(R.string.to_login))
            val clickableSpan: ClickableSpan = object : ClickableSpan() {
                override fun onClick(widget: View) {
                    redirectToLogin(null)
                }

                override fun updateDrawState(ds: TextPaint) {
                    super.updateDrawState(ds)
                    ds.color = Utils.getThemeAttribute(R.attr.themeContentTextColorAccent, root.context)
                    ds.isUnderlineText = false
                    ds.isFakeBoldText = true
                }
            }
            spannableString.setSpan(clickableSpan,
                resources.getInteger(R.integer.login_span_start),
                resources.getInteger(R.integer.login_span_end),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            tvToLogin.run {
                text = spannableString
                highlightColor = Color.TRANSPARENT
                movementMethod = LinkMovementMethod.getInstance()
            }
        }
    }

    private fun initObserver(){
        registerViewModel.registerResponse.observe(this, {
            when(it.status){
                ResponseWrapper.Status.LOADING -> {
                    Log.d("LOADING", "YES")
                }
                ResponseWrapper.Status.SUCCESS -> {
                    redirectToLogin(it.body?.data?.email.orEmpty())
                }
                else -> {
                    Toast.makeText(this, "ERROR: "+it.body?.errors.toString(), Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun callRegisterApi(){
        val fullName = viewBinding.layoutFieldName.etInput.text.toString()
        val email = viewBinding.layoutFieldEmail.etInput.text.toString().trim()
        val password = viewBinding.layoutFieldPassword.etInput.text.toString()
        val confirmPassword = viewBinding.layoutFieldConfirmPassword.etInput.text.toString()
        val registerRequest = RegisterRequest(fullName=fullName, email = email, password = password)
        if (fullName.isEmpty() || email.isEmpty() || password.isEmpty()){
            Toast.makeText(this, resources.getString(R.string.all_field_must_be_filled), Toast.LENGTH_SHORT).show()
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            Toast.makeText(this, resources.getString(R.string.wrong_email_format), Toast.LENGTH_SHORT).show()
        } else if (password != confirmPassword){
            Toast.makeText(this, resources.getString(R.string.password_not_matched), Toast.LENGTH_SHORT).show()
        } else {
            registerViewModel.callRegisterApi(registerRequest)
        }
    }

    private fun redirectToLogin(email: String?){
        val intent = Intent(this, LoginActivity::class.java)
        email?.let { intent.putExtra(LoginActivity.EMAIL, it) }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }
}