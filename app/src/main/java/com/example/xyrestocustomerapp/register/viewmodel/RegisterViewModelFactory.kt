package com.example.xyrestocustomerapp.register.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.xyrestocustomerapp.register.repository.RegisterRepository

class RegisterViewModelFactory(private val registerRepository: RegisterRepository): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return RegisterViewModel(registerRepository) as T
    }
}