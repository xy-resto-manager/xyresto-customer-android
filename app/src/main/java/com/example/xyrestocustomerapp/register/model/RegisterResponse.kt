package com.example.xyrestocustomerapp.register.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RegisterResponse(
    @field:SerializedName("userId")
    val userId: Int? = null,
    @field:SerializedName("fullName")
    val fullName: String? = null,
    @field:SerializedName("email")
    val email: String? = null,
    @field:SerializedName("password")
    val password: String? = null,
    @field:SerializedName("roles")
    val roles: List<String>? = null
): Parcelable
