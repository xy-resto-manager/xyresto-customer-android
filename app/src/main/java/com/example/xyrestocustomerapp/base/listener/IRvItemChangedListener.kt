package com.example.xyrestocustomerapp.base.listener

interface IRvItemChangedListener {
    fun triggerItemChanged(position: Int)
}