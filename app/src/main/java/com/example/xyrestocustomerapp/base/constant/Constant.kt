package com.example.xyrestocustomerapp.base.constant

object Constant {
    const val SERVER_URL = "http://xyresto-customer.maximuse.xyz:8080"
    const val BASE_URL = "$SERVER_URL/api/"

    //shared preference key
    const val PRODUCT_KEY = "product"
}