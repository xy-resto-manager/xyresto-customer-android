package com.example.xyrestocustomerapp.base.widget

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import com.example.xyrestocustomerapp.R
import com.example.xyrestocustomerapp.base.utils.Utils
import com.google.android.material.bottomnavigation.BottomNavigationView

class CustomBottomNavigationView  @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : BottomNavigationView(context, attrs, defStyleAttr){

    private val mPath: Path = Path()
    private val mPaint: Paint = Paint()
    private val fabRadius: Int = 60
    // the coordinates of center side
    private val centerPoint1 = Point()
    private val centerPoint2 = Point()
    private val centerPoint3 = Point()
    private val centerPoint4 = Point()
    private var mNavigationBarWidth = 0
    private var mNavigationBarHeight = 0

    companion object {
        private const val TRAPEZIUM_SHAPE_HEIGHT_ON_NAV = 25
    }

    init {
        with(mPaint) {
            style = Paint.Style.FILL_AND_STROKE
            color = Utils.getThemeAttribute(R.attr.themeBottomNavBackgroundColor, context)
        }
        setBackgroundColor(Color.TRANSPARENT)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        // get width and height of navigation bar
        mNavigationBarWidth = width
        mNavigationBarHeight = height
        // lower left point of center side
        centerPoint1.set(mNavigationBarWidth / 2 - 2 * fabRadius, TRAPEZIUM_SHAPE_HEIGHT_ON_NAV)
        // upper left point of center side
        centerPoint2.set(mNavigationBarWidth / 2 - fabRadius, 0)
        // upper right point of center side
        centerPoint3.set(mNavigationBarWidth / 2 + fabRadius, 0)
        // lower right point of center side
        centerPoint4.set(mNavigationBarWidth / 2 + 2 * fabRadius, TRAPEZIUM_SHAPE_HEIGHT_ON_NAV)

        mPath.apply {
            reset()
            moveTo(0f, TRAPEZIUM_SHAPE_HEIGHT_ON_NAV.toFloat())
            lineTo(centerPoint1.x.toFloat(), centerPoint1.y.toFloat())
            lineTo(centerPoint2.x.toFloat(), centerPoint2.y.toFloat())
            lineTo(centerPoint3.x.toFloat(), centerPoint3.y.toFloat())
            lineTo(centerPoint4.x.toFloat(), centerPoint4.y.toFloat())
            lineTo(mNavigationBarWidth.toFloat(), TRAPEZIUM_SHAPE_HEIGHT_ON_NAV.toFloat())
            lineTo(mNavigationBarWidth.toFloat(), mNavigationBarHeight.toFloat())
            lineTo(0f, mNavigationBarHeight.toFloat())
            close()
        }
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas?.drawPath(mPath, mPaint)
    }
}