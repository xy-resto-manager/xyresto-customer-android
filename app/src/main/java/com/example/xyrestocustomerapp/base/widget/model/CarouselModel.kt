package com.example.xyrestocustomerapp.base.widget.model

abstract class CarouselModel {
    abstract val imageUrl: String
}