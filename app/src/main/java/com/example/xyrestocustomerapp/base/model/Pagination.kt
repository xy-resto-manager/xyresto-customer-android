package com.example.xyrestocustomerapp.base.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Pagination(
    @field:SerializedName("currentPage")
    val currentPage: Int = 0,

    @field:SerializedName("itemsPerPage")
    val itemsPerPage: Int = 0,

    @field:SerializedName("totalItems")
    val totalItems: Int = 0
): Parcelable
