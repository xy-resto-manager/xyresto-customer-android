package com.example.xyrestocustomerapp.base.storage.session

import android.content.Context
import android.content.SharedPreferences

abstract class BaseSharedPreference<T>(context: Context){
    companion object {
        const val SHARED_PREFERENCES = "shared_preferences"
        const val USER_TOKEN = "user_token"
        const val PRODUCT_KEY = "product"
    }
    protected val prefs: SharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE)

    abstract fun saveData(data: T)

    abstract fun getData(): T

    fun removeData(key: String) {
        prefs.edit().remove(key).apply()
    }

    fun clearSharedPreference() {
        prefs.edit().clear().apply()
    }
}