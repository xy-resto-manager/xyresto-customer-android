package com.example.xyrestocustomerapp.base.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Category(
    @field:SerializedName("id")
    val id: Int,
    @field:SerializedName("name")
    val name: String? = null
): Parcelable
