package com.example.xyrestocustomerapp.base.widget

import android.content.Context
import android.os.Handler
import android.os.Message
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import com.example.xyrestocustomerapp.R
import com.example.xyrestocustomerapp.base.widget.adapter.CarouselAdapter
import java.lang.ref.WeakReference

class CarouselView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : RecyclerView(context, attrs, defStyleAttr) {

    companion object {
        const val WHAT_SCROLL = 1
    }

    private val scrollHandler by lazy {
        ScrollHandler(this)
    }

    private val delayMillis: Long
    private var itemCount = 0

    init {
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.CarouselView,
            0, 0
        ).apply {
            try {
                delayMillis = getInt(R.styleable.CarouselView_delay, 3000).toLong()
            } finally {
                recycle()
            }
        }
    }

    private fun createScroller(position: Int) = object : LinearSmoothScroller(context) {
        override fun getHorizontalSnapPreference(): Int {
            return SNAP_TO_END
        }
    }.apply {
        targetPosition = position
    }

    override fun dispatchTouchEvent(e: MotionEvent?): Boolean {
        when (e?.action) {
            MotionEvent.ACTION_UP -> {
                resumeAutoScroll()
                parent.requestDisallowInterceptTouchEvent(true)
            }
            MotionEvent.ACTION_DOWN -> {
                pauseAutoScroll()
                parent.requestDisallowInterceptTouchEvent(false)
            }
        }
        parent.requestDisallowInterceptTouchEvent(true)

        return super.dispatchTouchEvent(e)
    }

    fun pauseAutoScroll() {
        scrollHandler.removeMessages(WHAT_SCROLL)
    }

    fun resumeAutoScroll() {
        scrollHandler.removeMessages(WHAT_SCROLL)
        scrollHandler.sendEmptyMessageDelayed(WHAT_SCROLL, delayMillis)
    }

    fun scrollNext(position: Int? = null) {
        itemCount = (adapter as CarouselAdapter<*>).actualItemCount
        (layoutManager as? LinearLayoutManager)?.let { layoutManager ->
            position?.let {
                layoutManager.startSmoothScroll(
                    createScroller(position)
                )
            } ?: run {
                layoutManager.startSmoothScroll(
                    createScroller(layoutManager.findLastVisibleItemPosition() % itemCount)
                )
                scrollHandler.sendEmptyMessageDelayed(WHAT_SCROLL, delayMillis)
            }
        }
    }

    private class ScrollHandler(carouselView: CarouselView) :
        Handler() {

        private val autoScrollViewPager =
            WeakReference<CarouselView>(carouselView)

        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)
            autoScrollViewPager.get()?.scrollNext()
        }
    }
}