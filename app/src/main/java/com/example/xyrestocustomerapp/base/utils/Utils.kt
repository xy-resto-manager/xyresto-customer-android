package com.example.xyrestocustomerapp.base.utils

import android.content.Context
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan
import android.util.TypedValue
import com.example.xyrestocustomerapp.base.model.DateHolder
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

object Utils {
    fun getThemeAttribute(themeAttribute: Int, context: Context): Int {
        val typedValue = TypedValue()
        val theme = context.theme
        theme.resolveAttribute(themeAttribute, typedValue, true)
        return typedValue.data
    }

    fun Int?.orDefaultInt(defaultInt: Int? = null): Int = this ?: defaultInt ?: 0

    fun ceilInt(number1: Int, number2: Int): Int {
        return if (number1%number2 == 0) {
            number1/number2
        } else {
            (number1/number2)+1
        }
    }

    fun Int?.formatRp(defaultInt: Int? = null): String {
        val priceSplit = this.toString().toList()
        var result = "Rp. "
        var i = 1
        if (priceSplit.size > 3){
            for (c in priceSplit){
                if (i == priceSplit.size % 3 || (i - (priceSplit.size % 3)) % 3 == 0){
                    result += c
                    if (i < priceSplit.size)
                        result += "."
                } else {
                    result += c
                }
                i++
            }
        } else {
            result += this ?: defaultInt ?: 0
        }
        return result
    }

    fun Boolean?.orDefaultBool(defaultBool: Boolean? = null): Boolean = this ?: defaultBool ?: false

    fun Int.formatTime(): String {
        return if (this < 10){
            "0${this}"
        } else {
            this.toString()
        }
    }

    fun String.formatTime(): Int {
        val trim = this.split("")
        return if (trim[0] == "0"){
            trim[1].toInt()
        } else {
            this.toInt()
        }
    }

    fun getSeparateDate(): List<Int>{
        val calendar = Calendar.getInstance(TimeZone.getDefault())
        val separateDate: List<Int> = listOf(
            calendar.get(Calendar.DATE),
            calendar.get(Calendar.MONTH)+1,
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.HOUR_OF_DAY),
            calendar.get(Calendar.MINUTE)
        )
        return separateDate
    }

    fun formatDate(year: Int, month: Int, dayOfMonth: Int): String{
        val calendar = Calendar.getInstance()
        calendar.set(year, month, dayOfMonth)
        val dayName = when(calendar.get(Calendar.DAY_OF_WEEK)){
            1 -> "Sun"
            2 -> "Mon"
            3 -> "Tue"
            4 -> "Wed"
            5 -> "Thu"
            6 -> "Fri"
            else -> "Sat"
        }
        val monthName = when(month){
            0 -> "Jan"
            1 -> "Feb"
            2 -> "Mar"
            3 -> "Apr"
            4 -> "May"
            5 -> "Jun"
            6 -> "Jul"
            7 -> "Aug"
            8 -> "Sep"
            9 -> "Oct"
            10 -> "Nov"
            else -> "Dec"
        }

        return "$dayName, $dayOfMonth $monthName $year"
    }

    fun getCurrentDate(): String{
        val c: Date = Calendar.getInstance().time
        val df = SimpleDateFormat("E, dd MMM yyyy", Locale.getDefault())
        val formattedDate: String = df.format(c)
        return formattedDate
    }

    fun getCurrentTime(): String{
        val c: Date = Calendar.getInstance().time
        val df = SimpleDateFormat("HH:mm", Locale.getDefault())
        val formattedTime: String = df.format(c)
        return formattedTime
    }

    fun rangeTime(timeStamp: Long, hourRange: Int = 2): String{
        val dateHolder = timeStampToDateHolder(timeStamp)
        dateHolder.run {
            return hourOfDay.formatTime() + ":" + minute.formatTime() + "-" + ((hourOfDay+hourRange)%24).formatTime() + ":" + minute.formatTime()
        }
    }

    fun timeStampToDateHolder(timeStamp: Long): DateHolder{
        val dates = SimpleDateFormat("yyyy-MM-dd-HH-mm", Locale.getDefault()).format(timeStamp)
        val separateDate = dates.split("-")
        val dateHolder = DateHolder(
            separateDate[0].toInt(),
            separateDate[1].toInt(),
            separateDate[2].toInt(),
            separateDate[3].toInt(),
            separateDate[4].toInt()
        )
        return dateHolder
    }

    fun getTimeStampFormat(date: String, pattern: String = "dd-MM-yyyy HH:mm"): Long{
        val formatter: DateFormat = SimpleDateFormat(pattern, Locale.getDefault())
        val newDate: Date = formatter.parse(date)!!
        return newDate.time
    }

    fun timeStampToDate(timeStamp: Long, pattern: String = "E, dd MMM yyyy"): String{
        return SimpleDateFormat(pattern, Locale.getDefault()).format(timeStamp)
    }

    fun getSpannableBold(str: String, start: Int, end: Int): SpannableStringBuilder {
        val spannableString = SpannableStringBuilder(str)
        val styleSpan =  StyleSpan(Typeface.BOLD)
        spannableString.setSpan(styleSpan, start, end, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)
        return spannableString
    }
}