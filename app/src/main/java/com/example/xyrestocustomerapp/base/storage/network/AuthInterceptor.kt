package com.example.xyrestocustomerapp.base.storage.network

import android.content.Context
import android.content.Intent
import android.util.Log
import com.example.xyrestocustomerapp.base.storage.session.SessionManager
import com.example.xyrestocustomerapp.login.view.LoginActivity
import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor(private val context: Context) : Interceptor {
    private val sessionManager = SessionManager(context)

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        if (request.url.encodedPath.contains("user/current-user") ||
            request.url.encodedPath.contains("user/_change-password") ||
            request.url.encodedPath.contains("cart") ||
            request.url.encodedPath.contains("reservation")){
            val requestBuilder = chain.request().newBuilder()
            sessionManager.getData()?.let {
                requestBuilder.addHeader("Authorization", "Bearer $it")
            }
            val response = chain.proceed(requestBuilder.build())
            if (response.code == 403 || (request.url.encodedPath.contains("user/current-user") && response.code == 400))
                context.startActivity(Intent(context, LoginActivity::class.java))
            return response
        }
        return chain.proceed(request)
    }

}