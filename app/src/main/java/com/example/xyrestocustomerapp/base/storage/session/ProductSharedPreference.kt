package com.example.xyrestocustomerapp.base.storage.session

import android.content.Context
import com.example.xyrestocustomerapp.base.model.Product
import com.google.gson.Gson

class ProductSharedPreference(context: Context): BaseSharedPreference<Product?>(context) {
    override fun saveData(data: Product?) {
        data?.let {
            val editor = prefs.edit()
            editor.putString(PRODUCT_KEY, Gson().toJson(it))
            editor.apply()
        }
    }

    override fun getData(): Product? {
        return Gson().fromJson(prefs.getString(PRODUCT_KEY, null), Product::class.java)
    }
}