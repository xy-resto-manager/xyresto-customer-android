package com.example.xyrestocustomerapp.base.storage.session

import android.content.Context
import android.content.SharedPreferences

class SessionManager(context: Context): BaseSharedPreference<String?>(context) {

    override fun saveData(data: String?) {
        val editor = prefs.edit()
        editor.putString(USER_TOKEN, data)
        editor.apply()
    }

    override fun getData(): String? {
        return prefs.getString(USER_TOKEN, null)
    }

}