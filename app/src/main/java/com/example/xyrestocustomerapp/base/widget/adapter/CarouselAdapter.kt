package com.example.xyrestocustomerapp.base.widget.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners
import com.example.xyrestocustomerapp.R
import com.example.xyrestocustomerapp.base.constant.Constant.SERVER_URL
import com.example.xyrestocustomerapp.base.widget.model.CarouselModel
import kotlinx.android.synthetic.main.carousel_holder.view.*

class CarouselAdapter<T : CarouselModel>() : RecyclerView.Adapter<CarouselAdapter.CarouselHolder<T>>() {
    private var items: List<T> = listOf()
    val actualItemCount
        get() = items.size
    private var screenWidth: Int = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarouselHolder<T> {
        val displayMetrics = parent.context.resources.displayMetrics
        screenWidth = displayMetrics.widthPixels
        LayoutInflater.from(parent.context).inflate(R.layout.carousel_holder, parent, false)
            .let {
                return CarouselHolder(it)
            }
    }

    override fun getItemCount(): Int = if (items.isEmpty()) 0 else Integer.MAX_VALUE

    override fun onBindViewHolder(holder: CarouselHolder<T>, position: Int) {
        val itemWidth = (screenWidth*0.8).toInt()
        val lp = holder.itemView.layoutParams
        lp.width = itemWidth
        lp.height = lp.height
        holder.itemView.layoutParams = lp
        items[position % actualItemCount].let(holder::bind)
    }

    fun setItems(value: List<T>) {
        items = value
        notifyDataSetChanged()
    }

    class CarouselHolder<T : CarouselModel>(
        itemView: View
    ) : RecyclerView.ViewHolder(itemView) {

        private val iv = itemView.iv_carousel_item

        fun bind(data: T) {
            Glide.with(itemView.context)
                .load(SERVER_URL+data.imageUrl)
                .transform(CenterCrop(), GranularRoundedCorners(10.0F, 10.0F, 10.0F, 10.0F))
                .placeholder(R.drawable.ic_food)
                .into(iv)
        }
    }
}