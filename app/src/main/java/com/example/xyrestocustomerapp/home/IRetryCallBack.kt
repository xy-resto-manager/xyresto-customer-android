package com.example.xyrestocustomerapp.home

interface IRetryCallBack {
    fun retry()
}