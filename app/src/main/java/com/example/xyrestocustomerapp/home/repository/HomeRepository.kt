package com.example.xyrestocustomerapp.home.repository

import com.example.xyrestocustomerapp.home.network.IHomeApiService

class HomeRepository(private val iHomeApiService: IHomeApiService) {

    suspend fun callHomeBannerApi() = iHomeApiService.getAllBanner()
    suspend fun callPopularProductsApi(page: Int = 1, query: String? = null) =  iHomeApiService.getPopularProduct(page=page, query=query)
    suspend fun callRecommendedProductsApi(page: Int = 1, query: String? = null) = iHomeApiService.getRecommendedProduct(page=page, query=query)

}