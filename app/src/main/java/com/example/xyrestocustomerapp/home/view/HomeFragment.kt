package com.example.xyrestocustomerapp.home.view

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.xyrestocustomerapp.R
import com.example.xyrestocustomerapp.base.listener.PaginationListener
import com.example.xyrestocustomerapp.base.model.Product
import com.example.xyrestocustomerapp.base.storage.network.ApiClient
import com.example.xyrestocustomerapp.base.utils.ResponseWrapper
import com.example.xyrestocustomerapp.base.utils.Utils.ceilInt
import com.example.xyrestocustomerapp.base.utils.Utils.orDefaultInt
import com.example.xyrestocustomerapp.base.widget.adapter.CarouselAdapter
import com.example.xyrestocustomerapp.base.widget.decoration.SpacingItemDecoration
import com.example.xyrestocustomerapp.base.widget.model.CarouselModel
import com.example.xyrestocustomerapp.cart.model.CartRequest
import com.example.xyrestocustomerapp.databinding.FragmentHomeBinding
import com.example.xyrestocustomerapp.home.IRetryCallBack
import com.example.xyrestocustomerapp.home.LoadingCommunicator
import com.example.xyrestocustomerapp.home.LoadingMode
import com.example.xyrestocustomerapp.home.adapter.ProductRecommendationAdapter
import com.example.xyrestocustomerapp.home.model.BannerResponse
import com.example.xyrestocustomerapp.home.network.IHomeApiService
import com.example.xyrestocustomerapp.home.repository.HomeRepository
import com.example.xyrestocustomerapp.home.viewmodel.HomeViewModel
import com.example.xyrestocustomerapp.home.viewmodel.HomeViewModelFactory

class HomeFragment : Fragment() {
    private var _viewBinding: FragmentHomeBinding? = null
    private val viewBinding get() = _viewBinding!!
    private val homeViewModel: HomeViewModel by viewModels{
        HomeViewModelFactory(HomeRepository(ApiClient.getRetrofitInstance(requireContext()).create(IHomeApiService::class.java)))
    }
    private val iHomeActivityCommunicator: IHomeActivityCommunicator by lazy { activity as IHomeActivityCommunicator }
    private val iLoadingCommunicator: LoadingCommunicator by lazy { activity as LoadingCommunicator }
    private val carouselAdapter by lazy { CarouselAdapter<CarouselModel>() }
    private val topProductAdapter by lazy { ProductRecommendationAdapter(::onPopularProductItemClicked, 0.45) }
    private val recommendedProductAdapter by lazy { ProductRecommendationAdapter(::onPopularProductItemClicked, 0.45) }
    private var totalPagePopularProduct: Int = 1
    private var totalPageRecommendedProduct: Int = 1
    private var currentPagePopularProduct: Int = 0
    private var currentPageRecommendedProduct: Int = 0
    private var isLastPagePopularProduct = false
    private var isLastPageRecommendedProduct = false
    private var isLoadingPopularProduct = false
    private var isLoadingRecommendedProduct = false
    private val retryCallbackList = mutableListOf<Pair<Int, IRetryCallBack>>()

    companion object{
        const val PRODUCT_POPULAR = "popular"
        const val PRODUCT_RECOMMENDED = "recommended"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _viewBinding = FragmentHomeBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initRecyclerView()
        initObserver()
        setPaginationOnPopularProduct()
        setPaginationOnRecommendedProduct()
    }

    private fun setPaginationOnPopularProduct(){
        with(viewBinding.layoutTopDishes.rvProductList){
            layoutManager?.let {
                addOnScrollListener(object: PaginationListener(it, 10){
                    override fun loadMoreItems() {
                        isLoadingPopularProduct = true
                        callPopularProductListApi(page = currentPagePopularProduct+1)
                    }

                    override fun isLastPage(): Boolean {
                        return isLastPagePopularProduct
                    }

                    override fun isLoading(): Boolean {
                        return isLoadingPopularProduct
                    }

                })
            }
        }
    }

    private fun setPaginationOnRecommendedProduct(){
        with(viewBinding.layoutChefRecommendation.rvProductList){
            layoutManager?.let {
                addOnScrollListener(object: PaginationListener(it, 10){
                    override fun loadMoreItems() {
                        isLoadingRecommendedProduct = true
                        callRecommendedProductListApi( page = currentPageRecommendedProduct+1)
                    }

                    override fun isLastPage(): Boolean {
                        return isLastPageRecommendedProduct
                    }

                    override fun isLoading(): Boolean {
                        return isLoadingRecommendedProduct
                    }

                })
            }
        }
    }

    private fun setCarouselView(bannerList: List<BannerResponse>?){
        with(viewBinding.rvCarousel) {
            onFlingListener = null
            PagerSnapHelper().attachToRecyclerView(this)
            adapter = carouselAdapter.apply {
                bannerList?.let { setItems(it) }
                addItemDecoration(SpacingItemDecoration(rightMargin = 20))
            }
            resumeAutoScroll()
            addOnScrollListener(object: RecyclerView.OnScrollListener(){
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                    notifyActiveItem()
                }
            })
        }

        if (carouselAdapter.actualItemCount > 0) {
            with(viewBinding.llDotSign){
                removeAllViews()
                for (i in 0 until carouselAdapter.actualItemCount) {
                    val imageView = ImageView(context)
                    val params = LinearLayout.LayoutParams(
                        40,
                        40
                    )
                    params.setMargins(2,2,2,2)
                    imageView.layoutParams = params
                    if (i == 0) {
                        imageView.background = ContextCompat.getDrawable(context, R.drawable.background_circle_yellow_light)
                    } else {
                        imageView.background = ContextCompat.getDrawable(context, R.drawable.background_circle_yellow_dark)
                    }
                    addView(imageView)
                    val dotPosition = childCount -1
                    imageView.setOnClickListener {
                        with(viewBinding.rvCarousel) {
                            pauseAutoScroll()
                            scrollNext(dotPosition)
                            resumeAutoScroll()
                        }
                    }
                }
            }
        }
    }

    private fun onPopularProductItemClicked(product: Product){
        iHomeActivityCommunicator.updateCart(CartRequest(product.id, 1))
    }

    private fun notifyActiveItem() {
        carouselAdapter.takeIf { it.itemCount > 0 }?.run {
            val itemCount = (this as CarouselAdapter<*>).actualItemCount

            (viewBinding.rvCarousel.layoutManager as? LinearLayoutManager)?.let { layoutManager ->
                val firstItemPosition = layoutManager.findFirstCompletelyVisibleItemPosition()
                val actualItemPosition = firstItemPosition % itemCount

                layoutManager.findViewByPosition(firstItemPosition)?.let { firstView ->
                    for (i in 0 until itemCount) {
                        with(viewBinding.llDotSign) {
                            when (i) {
                                actualItemPosition -> {
                                    getChildAt(actualItemPosition).background = ContextCompat.getDrawable(context, R.drawable.background_circle_yellow_light)
                                }
                                else -> {
                                    getChildAt(i).background = ContextCompat.getDrawable(context, R.drawable.background_circle_yellow_dark)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private fun callPopularProductListApi(page: Int = 1, query: String? = null){
        homeViewModel.getPopularProducts(page=page, query=query)
    }

    private fun callRecommendedProductListApi(page: Int = 1, query: String? = null){
        homeViewModel.getRecommendedProducts(page=page, query=query)
    }

    private fun initView(){
        with(viewBinding){
            layoutTopDishes.tvTitle.text = resources.getString(R.string.home_top_dishes)
            layoutTopDishes.tvSubtitle.text = resources.getString(R.string.home_top_dishes_desc)
            layoutChefRecommendation.tvTitle.text = resources.getString(R.string.home_chef_favourites)
            layoutChefRecommendation.tvSubtitle.text = resources.getString(R.string.home_chef_favourites_desc)
        }
    }

    private fun initRecyclerView(){
        with(viewBinding.layoutTopDishes.rvProductList){
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = topProductAdapter.apply {
                addItemDecoration(SpacingItemDecoration(leftMargin = 20))
            }
        }
        with(viewBinding.layoutChefRecommendation.rvProductList){
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = recommendedProductAdapter.apply {
                addItemDecoration(SpacingItemDecoration(leftMargin = 20))
            }
        }
    }

    private fun initObserver(){
        iLoadingCommunicator.startLoading(3, LoadingMode.PROGRESS_BAR_WITH_BACKGROUND)
        homeViewModel.banners.observe(viewLifecycleOwner, {
            retryCallbackList.removeAll { it.first == 0 }
            when(it.status){
                ResponseWrapper.Status.LOADING -> {
                    Log.d("LOADING", "YES")
                }
                ResponseWrapper.Status.SUCCESS -> {
                    setCarouselView(it.body?.data)
                    iLoadingCommunicator.updateLoadingProgress()
                }
                else -> {
                    with(iLoadingCommunicator){
                        updateLoadingProgress()
                        retryCallbackList.add(Pair(0, object : IRetryCallBack{
                            override fun retry() {
                                homeViewModel.getHomeBannerList()
                            }
                        }))
                        if (getProgress().first == getProgress().second){
                            iLoadingCommunicator.onError(it.body?.errors.toString(), retryCallbackList)
                        }
                    }
                }
            }
        })

        homeViewModel.popularProducts.observe(viewLifecycleOwner, {
            retryCallbackList.removeAll { it.first == 1 }
            when(it.status){
                ResponseWrapper.Status.LOADING -> {
                    Log.d("LOADING", "YES")
                }
                ResponseWrapper.Status.SUCCESS -> {
                    it.body?.pagination?.let {
                        currentPagePopularProduct = it.currentPage
                        totalPagePopularProduct = ceilInt(it.totalItems.orDefaultInt(1), it.itemsPerPage.orDefaultInt(1))
                    }
                    if (currentPagePopularProduct < totalPagePopularProduct) {
                        it.body?.data?.let { topProductAdapter.setItemList(it, false) }
                        topProductAdapter.addFooter(Product(), false)
                        topProductAdapter.notifyDataSetChanged()
                    } else {
                        isLastPagePopularProduct = true
                        topProductAdapter.removeFooter(false)
                        it.body?.data?.let { topProductAdapter.setItemList(it) }
                    }
                    iLoadingCommunicator.updateLoadingProgress()
                }
                else -> {
                    with(iLoadingCommunicator){
                        updateLoadingProgress()
                        retryCallbackList.add(Pair(1, object : IRetryCallBack{
                            override fun retry() {
                                callPopularProductListApi(page = currentPagePopularProduct+1)
                            }
                        }))
                        if (getProgress().first == getProgress().second){
                            iLoadingCommunicator.onError(it.body?.errors.toString(), retryCallbackList)
                        }
                    }
                }
            }
            isLoadingPopularProduct = false
        })
        homeViewModel.recommendedProducts.observe(viewLifecycleOwner, {
            retryCallbackList.removeAll { it.first == 2 }
            when(it.status){
                ResponseWrapper.Status.LOADING -> {

                }
                ResponseWrapper.Status.SUCCESS -> {
                    it.body?.pagination?.let {
                        currentPageRecommendedProduct = it.currentPage
                        totalPageRecommendedProduct = ceilInt(it.totalItems.orDefaultInt(1), it.itemsPerPage.orDefaultInt(1))
                    }
                    if (currentPageRecommendedProduct < totalPageRecommendedProduct) {
                        it.body?.data?.let { recommendedProductAdapter.setItemList(it, false) }
                        recommendedProductAdapter.addFooter(Product(), false)
                        recommendedProductAdapter.notifyDataSetChanged()
                    } else {
                        isLastPageRecommendedProduct = true
                        recommendedProductAdapter.removeFooter(false)
                        it.body?.data?.let { recommendedProductAdapter.setItemList(it) }
                    }
                    iLoadingCommunicator.updateLoadingProgress()
                }
                else -> {
                    with(iLoadingCommunicator){
                        updateLoadingProgress()
                        retryCallbackList.add(Pair(2, object : IRetryCallBack{
                            override fun retry() {
                                callRecommendedProductListApi( page = currentPageRecommendedProduct+1)
                            }
                        }))
                        if (getProgress().first == getProgress().second){
                            iLoadingCommunicator.onError(it.body?.errors.toString(), retryCallbackList)
                        }
                    }
                }
            }
            isLoadingRecommendedProduct = false
        })
    }

    override fun onStop() {
        super.onStop()
        viewBinding.rvCarousel.pauseAutoScroll()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _viewBinding = null
    }
}