package com.example.xyrestocustomerapp.home.view

import com.example.xyrestocustomerapp.base.storage.session.SessionManager
import com.example.xyrestocustomerapp.cart.model.CartRequest

interface IHomeActivityCommunicator {
    fun setActionBar(title: String, hideBackNav: Boolean)
    fun updateCart(cartRequest: CartRequest)
    fun getSessionManager(): SessionManager
}