package com.example.xyrestocustomerapp.home.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.example.xyrestocustomerapp.R
import com.example.xyrestocustomerapp.base.constant.Constant.PRODUCT_KEY
import com.example.xyrestocustomerapp.base.model.Product
import com.example.xyrestocustomerapp.base.storage.network.ApiClient
import com.example.xyrestocustomerapp.base.storage.session.ProductSharedPreference
import com.example.xyrestocustomerapp.base.storage.session.SessionManager
import com.example.xyrestocustomerapp.base.utils.ResponseWrapper
import com.example.xyrestocustomerapp.cart.model.CartRequest
import com.example.xyrestocustomerapp.cart.network.ICartApiService
import com.example.xyrestocustomerapp.cart.repository.CartRepository
import com.example.xyrestocustomerapp.cart.viewmodel.UpdateCartViewModel
import com.example.xyrestocustomerapp.cart.viewmodel.UpdateCartViewModelFactory
import com.example.xyrestocustomerapp.databinding.ActivityHomeBinding
import com.example.xyrestocustomerapp.home.IRetryCallBack
import com.example.xyrestocustomerapp.home.LoadingCommunicator
import com.example.xyrestocustomerapp.home.LoadingMode
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext
import kotlin.system.exitProcess

class HomeActivity : AppCompatActivity(), IHomeActivityCommunicator,LoadingCommunicator, CoroutineScope {

    private lateinit var viewBinding: ActivityHomeBinding
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main
    private lateinit var job: Job
    private lateinit var sessionManager: SessionManager
    private lateinit var productSharedPreference: ProductSharedPreference
    private val viewModel: UpdateCartViewModel by viewModels {
        UpdateCartViewModelFactory(
            CartRepository(
                ApiClient.getRetrofitInstance(this).create(
                    ICartApiService::class.java))
        )
    }
    private var maxLoadingTime = 0
    private var loadingProgress = 0
    private var loadingMode: LoadingMode = LoadingMode.PROGRESS_BAR_WITH_BACKGROUND

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)
        job = Job()
        sessionManager = SessionManager(this)
        productSharedPreference = ProductSharedPreference(this)
        initObserver()
        initNavigationView()
        initErrorView()
    }

    override fun onStart() {
        super.onStart()
        launch(Dispatchers.IO) {
            productSharedPreference.getData()?.let {
                if (intent.flags != Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    viewModel.callUpdateCurrentCartApi(CartRequest(it.id, 1))
                productSharedPreference.removeData(PRODUCT_KEY)
            }
        }
    }

    private fun initNavigationView(){
        with(viewBinding) {
            val navController = findNavController(R.id.nav_host_fragment)
            customBottomNav.setupWithNavController(navController)
            navController.addOnDestinationChangedListener { _, destination, _ ->
                when(destination.id){
                    R.id.option_reservation, R.id.option_reservation_detail,
                    R.id.option_reservation_step1, R.id.option_reservation_step2 -> {
                        layoutActionBar.appBar.visibility = View.VISIBLE
                        fabReservation.setIconResource(R.drawable.ic_vector_contained_menu_reservation)
                        customBottomNav.menu.findItem(R.id.option_reservation).isChecked = true
                    }
                    R.id.option_preorder -> {
                        layoutActionBar.appBar.visibility = View.VISIBLE
                        fabReservation.setIconResource(R.drawable.ic_vector_outlined_menu_reservation)
                    }
                    else -> {
                        layoutActionBar.appBar.visibility = View.GONE
                        fabReservation.setIconResource(R.drawable.ic_vector_outlined_menu_reservation)
                    }
                }
                layoutActionBar.run {
                    when(destination.id){
                        R.id.option_reservation -> {
                            setActionBar(resources.getString(R.string.reservations), true)
                        }
                        R.id.option_reservation_detail, R.id.option_reservation_step2 -> {
                            setActionBar(resources.getString(R.string.reservation_detail), false)
                        }
                        R.id.option_reservation_step1 -> {
                            setActionBar(resources.getString(R.string.make_reservation), false)
                        }
                        R.id.option_preorder -> {
                            setActionBar(resources.getString(R.string.preorder_list), true)
                        }
                    }
                }
            }
            fabReservation.setOnClickListener {
                navController.navigate(R.id.option_reservation)
                customBottomNav.findViewById<View>(R.id.option_reservation).performClick()
            }
            setupActionBar()
        }
    }

    private fun initErrorView(){
        with(viewBinding){
            btnExit.setOnClickListener {
                groupError.visibility = View.GONE
                finish()
                exitProcess(0)
            }
        }
    }

    private fun initObserver(){
        viewModel.cartResponse.observe(this, { event ->
            event.getContentIfNotHandled()?.let {
                when(it.status){
                    ResponseWrapper.Status.LOADING -> {

                    }
                    ResponseWrapper.Status.SUCCESS -> {
                        Toast.makeText(this, "Added Into Cart", Toast.LENGTH_SHORT).show()
                        launch(Dispatchers.IO){
                            productSharedPreference.removeData(PRODUCT_KEY)
                        }
                    }
                    ResponseWrapper.Status.ERROR -> {
                        Toast.makeText(this, "ERROR: "+ it.body?.errors, Toast.LENGTH_SHORT).show()
                    }
                    else -> {
                        Toast.makeText(this, "FATAL ERROR: "+ it.message, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        })
    }

    private fun setupActionBar(){
        with(viewBinding.layoutActionBar){
            toolbar.title = ""
            setSupportActionBar(toolbar)
            ivBtnBack.setOnClickListener { findNavController(R.id.nav_host_fragment).popBackStack() }
        }
    }

    override fun setActionBar(title: String, hideBackNav: Boolean) {
        viewBinding.layoutActionBar.tvTitle.text = title
        if (hideBackNav)
            viewBinding.layoutActionBar.ivBtnBack.visibility = View.GONE
        else
            viewBinding.layoutActionBar.ivBtnBack.visibility = View.VISIBLE
    }

    override fun updateCart(cartRequest: CartRequest) {
        launch(Dispatchers.IO) {
            productSharedPreference.saveData(Product(id = cartRequest.productId))
        }
        viewModel.callUpdateCurrentCartApi(cartRequest)
    }

    override fun getSessionManager(): SessionManager {
        return sessionManager
    }

    override fun startLoading(maxIteration: Int, mode: LoadingMode) {
        loadingMode = mode
        maxLoadingTime = maxIteration
        loadingProgress = 0
        toggleErrorMessage(null, false)
        toggleLoadingVisibility(true)
    }

    override fun updateLoadingProgress() {
        loadingProgress++
        if (loadingProgress == maxLoadingTime){
            stopLoading()
        }
    }

    override fun getProgress(): Pair<Int,Int> {
        return Pair(loadingProgress, maxLoadingTime)
    }

    override fun stopLoading() {
        toggleLoadingVisibility(false)
    }

    override fun onError(message: String, callbacks: List<Pair<Int, IRetryCallBack>>) {
        toggleErrorMessage(message, true)
        viewBinding.btnRetry.setOnClickListener {
            viewBinding.groupError.visibility = View.GONE
            startLoading(callbacks.size, LoadingMode.PROGRESS_BAR_WITH_BACKGROUND)
            callbacks.forEach { callback ->
                callback.second.retry()
            }
        }
    }

    private fun toggleErrorMessage(message: String?, isVisible: Boolean){
        with(viewBinding){
            if (isVisible){
                loadingProgress = 0
                maxLoadingTime = -1
                toggleLoadingVisibility(isVisible = false, hideViewBackground = false)
                viewBackground.background = ContextCompat.getDrawable(root.context, android.R.color.white)
                message?.let { tvErrorMessage.text = it }
                groupError.visibility = View.VISIBLE
            } else {
                groupError.visibility = View.GONE
            }
        }
    }

    private fun toggleLoadingVisibility(isVisible: Boolean, hideViewBackground: Boolean = true){
        with(viewBinding){
            if (isVisible){
                if (loadingMode == LoadingMode.ONLY_PROGRESS_BAR)
                    viewBackground.background = ContextCompat.getDrawable(root.context, android.R.color.transparent)
                else
                    viewBackground.background = ContextCompat.getDrawable(root.context, android.R.color.white)
                viewBackground.visibility = View.VISIBLE
                progressBar.visibility = View.VISIBLE
            } else {
                if (hideViewBackground)
                    viewBackground.visibility = View.GONE
                progressBar.visibility = View.GONE
            }
        }
    }

    override fun onDestroy() {
        job.cancel()
        super.onDestroy()
    }
}