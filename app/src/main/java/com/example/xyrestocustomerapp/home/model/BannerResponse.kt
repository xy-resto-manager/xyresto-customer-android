package com.example.xyrestocustomerapp.home.model

import android.os.Parcelable
import com.example.xyrestocustomerapp.base.widget.model.CarouselModel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BannerResponse(
    val id: Int? = null,
    override val imageUrl: String
): CarouselModel(), Parcelable
