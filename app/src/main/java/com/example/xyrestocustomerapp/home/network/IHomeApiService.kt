package com.example.xyrestocustomerapp.home.network

import com.example.xyrestocustomerapp.base.model.BaseApiResponse
import com.example.xyrestocustomerapp.base.model.Product
import com.example.xyrestocustomerapp.home.model.BannerResponse
import com.example.xyrestocustomerapp.home.view.HomeFragment
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface IHomeApiService {
    //banner
    @GET("banner/banners")
    suspend fun getAllBanner(): Response<BaseApiResponse<List<BannerResponse>>>

    @GET("product/products")
    suspend fun getPopularProduct(
        @Query("page") page: Int? = null,
        @Query("sort") sort: String = HomeFragment.PRODUCT_POPULAR,
        @Query("query") query: String? = null
    ): Response<BaseApiResponse<List<Product>>>

    @GET("product/products")
    suspend fun getRecommendedProduct(
        @Query("page") page: Int? = null,
        @Query("sort") sort: String = HomeFragment.PRODUCT_RECOMMENDED,
        @Query("query") query: String? = null
    ): Response<BaseApiResponse<List<Product>>>
}