package com.example.xyrestocustomerapp.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners
import com.example.xyrestocustomerapp.R
import com.example.xyrestocustomerapp.base.adapter.FooterAdapter
import com.example.xyrestocustomerapp.base.adapter.LoadingHolder
import com.example.xyrestocustomerapp.base.constant.Constant.SERVER_URL
import com.example.xyrestocustomerapp.base.model.Product
import com.example.xyrestocustomerapp.base.utils.Utils.formatRp
import com.example.xyrestocustomerapp.databinding.HomeProductRecomItemBinding

class ProductRecommendationAdapter(
    private val clicked: (Product) -> Unit,
    private val itemWidthRatio: Double? = null
    ): FooterAdapter<Product>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseRvViewHolder<Product> {
        initScreenWidth(parent.context)
        when(viewType){
            VIEW_TYPE_CONTENT -> {
                val productItemBinding = HomeProductRecomItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                return ProductRecommendationHolder(productItemBinding)
            }
            else -> {
                LayoutInflater.from(parent.context).inflate(R.layout.loading_item, parent, false)
                    .let {
                        return LoadingHolder(it)
                    }
            }
        }
    }

    override fun onBindViewHolder(holder: BaseRvViewHolder<Product>, position: Int) {
        itemWidthRatio?.let {
            val itemWidth = (screenWidth*it).toInt()
            val lp = holder.itemView.layoutParams
            lp.width = itemWidth
            lp.height = lp.height
            holder.itemView.layoutParams = lp
        }
        mutableItems[position % mutableItemCount].let(holder::bind)
    }

    override fun getItemViewType(position: Int): Int {
        return if (isFooterVisible && position == mutableItemCount -1) {
            VIEW_TYPE_FOOTER
        } else {
            VIEW_TYPE_CONTENT
        }
    }

    inner class ProductRecommendationHolder(
        private val binding: HomeProductRecomItemBinding
    ) : BaseRvViewHolder<Product>(binding.root) {

        override fun bind(data: Product, position: Int?) {
            with(binding){
                ivProduct.layout(0,0,0,0)
                Glide.with(root.context)
                    .load(SERVER_URL+data.imageUrl)
                    .transform(CenterCrop(), GranularRoundedCorners(10.0F, 10.0F, 0.0F, 0.0F))
                    .placeholder(R.drawable.ic_food)
                    .into(ivProduct)

                ivBtnAdd.setOnClickListener {
                    clicked(data)
                }

                tvProductTitle.text = data.name
                tvProductPrice.text = data.price.formatRp()
            }
        }
    }
}