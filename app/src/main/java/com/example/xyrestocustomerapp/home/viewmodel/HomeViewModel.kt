package com.example.xyrestocustomerapp.home.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.xyrestocustomerapp.base.model.BaseApiResponse
import com.example.xyrestocustomerapp.base.model.Product
import com.example.xyrestocustomerapp.base.utils.ApiCallHelper.safeApiCall
import com.example.xyrestocustomerapp.base.utils.ResponseWrapper
import com.example.xyrestocustomerapp.home.model.BannerResponse
import com.example.xyrestocustomerapp.home.repository.HomeRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HomeViewModel(private val homeRepository: HomeRepository): ViewModel() {
    private val _banners = MutableLiveData<ResponseWrapper<BaseApiResponse<List<BannerResponse>>>>()
    val banners: LiveData<ResponseWrapper<BaseApiResponse<List<BannerResponse>>>>
        get() = _banners
    private val _popularProducts = MutableLiveData<ResponseWrapper<BaseApiResponse<List<Product>>>>()
    val popularProducts: LiveData<ResponseWrapper<BaseApiResponse<List<Product>>>>
        get() = _popularProducts
    private val _recommendedProducts = MutableLiveData<ResponseWrapper<BaseApiResponse<List<Product>>>>()
    val recommendedProducts: LiveData<ResponseWrapper<BaseApiResponse<List<Product>>>>
        get() = _recommendedProducts

    init {
        getHomeBannerList()
        getPopularProducts(page = 1)
        getRecommendedProducts(page = 1)
    }

    fun getHomeBannerList() {
        viewModelScope.launch(Dispatchers.IO) {
            _banners.postValue(ResponseWrapper.loading())
            val result = safeApiCall { homeRepository.callHomeBannerApi() }
            _banners.postValue(result)
        }
    }

    fun getPopularProducts(page: Int = 1, query: String? = null) {
        viewModelScope.launch(Dispatchers.IO) {
            val result = safeApiCall { homeRepository.callPopularProductsApi(page = page, query=query) }
            _popularProducts.postValue(concatProductList(result, 0))
        }
    }

    fun getRecommendedProducts(page: Int = 1, query: String? = null) {
        viewModelScope.launch(Dispatchers.IO) {
            val result = safeApiCall { homeRepository.callRecommendedProductsApi(page = page, query=query) }
            _recommendedProducts.postValue(concatProductList(result, 1))
        }
    }

    private fun concatProductList(response: ResponseWrapper<BaseApiResponse<List<Product>>>, productType: Int): ResponseWrapper<BaseApiResponse<List<Product>>> {
        val productTemp: List<Product>? = if (productType == 0)
            _popularProducts.value?.body?.data
        else
            _recommendedProducts.value?.body?.data
        productTemp?.let { products ->
            response.body.let {
                return ResponseWrapper(
                    response.status,
                    BaseApiResponse(
                        code = it?.code,
                        data = products.toMutableList().apply { it?.data?.let { addAll(it) } },
                        status = it?.status,
                        errors = it?.errors,
                        pagination = it?.pagination.run {
                            when {
                                response.status == ResponseWrapper.Status.SUCCESS -> it?.pagination
                                productType == 0 -> _popularProducts.value?.body?.pagination
                                else -> _recommendedProducts.value?.body?.pagination
                            }

                        }
                    ),
                    response.message
                )
            }
        }
        return response
    }
}