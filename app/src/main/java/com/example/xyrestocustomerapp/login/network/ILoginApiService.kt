package com.example.xyrestocustomerapp.login.network

import com.example.xyrestocustomerapp.base.model.BaseApiResponse
import com.example.xyrestocustomerapp.base.model.Product
import com.example.xyrestocustomerapp.home.view.HomeFragment
import com.example.xyrestocustomerapp.login.model.LoginRequest
import com.example.xyrestocustomerapp.login.model.LoginResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface ILoginApiService {
    @POST("user/_login")
    suspend fun login(
        @Body loginRequest: LoginRequest? = null
    ): Response<BaseApiResponse<LoginResponse>>
}