package com.example.xyrestocustomerapp.login.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LoginResponse(
    @field:SerializedName("token")
    val token: String? = null
):Parcelable
