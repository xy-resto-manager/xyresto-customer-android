package com.example.xyrestocustomerapp.login.view

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.method.PasswordTransformationMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.xyrestocustomerapp.R
import com.example.xyrestocustomerapp.base.storage.network.ApiClient
import com.example.xyrestocustomerapp.base.storage.session.SessionManager
import com.example.xyrestocustomerapp.base.utils.ResponseWrapper
import com.example.xyrestocustomerapp.base.utils.Utils
import com.example.xyrestocustomerapp.databinding.ActivityLoginBinding
import com.example.xyrestocustomerapp.home.view.HomeActivity
import com.example.xyrestocustomerapp.login.model.LoginRequest
import com.example.xyrestocustomerapp.login.network.ILoginApiService
import com.example.xyrestocustomerapp.login.repository.LoginRepository
import com.example.xyrestocustomerapp.login.viewmodel.LoginViewModel
import com.example.xyrestocustomerapp.profile.ChangePasswordActivity
import com.example.xyrestocustomerapp.register.view.RegisterActivity


class LoginActivity : AppCompatActivity() {
    private lateinit var viewBinding: ActivityLoginBinding
    private val loginViewModel: LoginViewModel by lazy {
        LoginViewModel(LoginRepository(ApiClient.getRetrofitInstance(this).create(ILoginApiService::class.java)))
    }
    private var bundle: Bundle? = null
    private var isAuthenticated: Boolean = false

    companion object{
        const val TAG_PREVIOUS_DATA = "previous_data"
        const val EMAIL = "email"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)
        bundle = savedInstanceState
        initView()
        initObserver()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    private fun initView(){
        with(viewBinding){
            layoutFieldEmail.tvLabel.text = resources.getString(R.string.label_email)
            layoutFieldEmail.etInput.setText(intent.getStringExtra(EMAIL))
            layoutFieldPassword.tvLabel.text = resources.getString(R.string.label_password)
            layoutFieldPassword.etInput.transformationMethod = PasswordTransformationMethod.getInstance()
            tvForgotPassword.setOnClickListener {
                val newIntent = Intent(this@LoginActivity, ChangePasswordActivity::class.java)
                startActivity(newIntent)
            }
            btnLogin.setOnClickListener {
                callLoginApi()
            }
            val spannableString = SpannableString(resources.getString(R.string.to_register))
            val clickableSpan: ClickableSpan = object : ClickableSpan() {
                override fun onClick(widget: View) {
                    startActivity(Intent(this@LoginActivity, RegisterActivity::class.java))
                }

                override fun updateDrawState(ds: TextPaint) {
                    super.updateDrawState(ds)
                    ds.color = Utils.getThemeAttribute(R.attr.themeContentTextColorAccent, root.context)
                    ds.isUnderlineText = false
                    ds.isFakeBoldText = true
                }
            }
            spannableString.setSpan(clickableSpan,
                resources.getInteger(R.integer.register_span_start),
                resources.getInteger(R.integer.register_span_end),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            tvToRegister.run {
                text = spannableString
                highlightColor = Color.TRANSPARENT
                movementMethod = LinkMovementMethod.getInstance()
            }
        }
    }

    private fun initObserver(){
        loginViewModel.loginResponse.observe(this, {
            when(it.status){
                ResponseWrapper.Status.LOADING -> {
                    Log.d("LOADING", "YES")
                }
                ResponseWrapper.Status.SUCCESS -> {
                    isAuthenticated = true
                    SessionManager(this).saveData(it.body?.data?.token.toString())
                    onBackPressed()
                }
                else -> {
                    Toast.makeText(this, "ERROR: "+it.body?.errors, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun callLoginApi(){
        val email = viewBinding.layoutFieldEmail.etInput.text.toString().trim()
        val password = viewBinding.layoutFieldPassword.etInput.text.toString()
        val loginRequest = LoginRequest(email = email, password = password)
        if (email.isEmpty() || password.isEmpty()){
            Toast.makeText(this, resources.getString(R.string.empty_email_password), Toast.LENGTH_SHORT).show()
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            Toast.makeText(this, resources.getString(R.string.wrong_email_format), Toast.LENGTH_SHORT).show()
        } else {
            loginViewModel.callLoginApi(loginRequest)
        }
    }

    override fun onBackPressed() {
        if (isAuthenticated)
            super.onBackPressed()
        else
            startActivity(Intent(this, HomeActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
    }
}