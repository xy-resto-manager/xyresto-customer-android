package com.example.xyrestocustomerapp.login.repository

import com.example.xyrestocustomerapp.login.model.LoginRequest
import com.example.xyrestocustomerapp.login.network.ILoginApiService

class LoginRepository(private val iLoginApiService: ILoginApiService){
    suspend fun callLoginApi(loginRequest: LoginRequest) = iLoginApiService.login(loginRequest=loginRequest)
}