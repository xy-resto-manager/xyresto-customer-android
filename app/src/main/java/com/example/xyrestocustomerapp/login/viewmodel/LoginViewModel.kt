package com.example.xyrestocustomerapp.login.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.xyrestocustomerapp.base.model.BaseApiResponse
import com.example.xyrestocustomerapp.base.utils.ApiCallHelper.safeApiCall
import com.example.xyrestocustomerapp.base.utils.ResponseWrapper
import com.example.xyrestocustomerapp.login.model.LoginRequest
import com.example.xyrestocustomerapp.login.model.LoginResponse
import com.example.xyrestocustomerapp.login.repository.LoginRepository
import kotlinx.coroutines.launch

class LoginViewModel(private val loginRepository: LoginRepository): ViewModel(){
    private val _loginResponse = MutableLiveData<ResponseWrapper<BaseApiResponse<LoginResponse>>>()
    val loginResponse: LiveData<ResponseWrapper<BaseApiResponse<LoginResponse>>>
        get() = _loginResponse

    fun callLoginApi(loginRequest: LoginRequest) {
        viewModelScope.launch {
            _loginResponse.postValue(ResponseWrapper.loading())
            val result = safeApiCall{ loginRepository.callLoginApi(loginRequest) }
            _loginResponse.postValue(result)
        }
    }
}