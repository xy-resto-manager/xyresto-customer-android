package com.example.xyrestocustomerapp.menu.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.xyrestocustomerapp.menu.repository.MenuRepository

class MenuViewModelFactory(private val repository: MenuRepository): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MenuViewModel(repository) as T
    }
}