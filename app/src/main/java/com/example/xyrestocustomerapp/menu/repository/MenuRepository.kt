package com.example.xyrestocustomerapp.menu.repository

import com.example.xyrestocustomerapp.menu.network.IMenuApiService

class MenuRepository(private val iMenuApiService: IMenuApiService) {
    suspend fun callMenuCategoryApi() = iMenuApiService.getAllCategories()
    suspend fun callGetProducts(sort: String, query: String? = null, page: Int = 1) = iMenuApiService.getProducts(sort=sort, query = query, page = page)
}