package com.example.xyrestocustomerapp.menu.network

import com.example.xyrestocustomerapp.base.model.BaseApiResponse
import com.example.xyrestocustomerapp.base.model.Category
import com.example.xyrestocustomerapp.base.model.Product
import com.example.xyrestocustomerapp.home.view.HomeFragment
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface IMenuApiService {
    @GET("product/categories")
    suspend fun getAllCategories(): Response<BaseApiResponse<List<Category>>>

    @GET("product/products")
    suspend fun getProducts(
        @Query("page") page: Int? = 1,
        @Query("sort") sort: String,
        @Query("query") query: String? = null
    ): Response<BaseApiResponse<List<Product>>>
}