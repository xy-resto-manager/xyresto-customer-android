package com.example.xyrestocustomerapp.menu.viewmodel

import androidx.lifecycle.*
import com.example.xyrestocustomerapp.base.model.BaseApiResponse
import com.example.xyrestocustomerapp.base.model.Category
import com.example.xyrestocustomerapp.base.model.Product
import com.example.xyrestocustomerapp.base.utils.ApiCallHelper.safeApiCall
import com.example.xyrestocustomerapp.base.utils.ResponseWrapper
import com.example.xyrestocustomerapp.base.utils.Utils.orDefaultInt
import com.example.xyrestocustomerapp.home.view.HomeFragment
import com.example.xyrestocustomerapp.menu.repository.MenuRepository
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MenuViewModel(private val menuRepository: MenuRepository): ViewModel() {
    private val _productCategories = MutableLiveData<ResponseWrapper<BaseApiResponse<List<Category>>>>()
    val productCategories: LiveData<ResponseWrapper<BaseApiResponse<List<Category>>>>
        get() = _productCategories
    private val _products = MutableLiveData<ResponseWrapper<BaseApiResponse<List<Product>>>>()
    val products: LiveData<ResponseWrapper<BaseApiResponse<List<Product>>>> get() = _products
    private var debouncePeriod: Long = 800
    private var searchJob: Job? = null

    init {
        callMenuCategoryApi()
        callProductByCategoryApi(sort = HomeFragment.PRODUCT_POPULAR)
    }

    fun callMenuCategoryApi() {
        viewModelScope.launch {
            _productCategories.postValue(ResponseWrapper.loading())
            val result = safeApiCall {menuRepository.callMenuCategoryApi()}
            _productCategories.postValue(result)
        }
    }

    fun callProductByCategoryApi(sort: String, query: String? = null, page: Int = 1) {
        searchJob?.cancel()
        searchJob = viewModelScope.launch {
            delay(debouncePeriod)
            val result = safeApiCall {menuRepository.callGetProducts(sort=sort, query=query, page=page)}
            _products.postValue(concatProductList(result))
        }
    }

    private fun concatProductList(response: ResponseWrapper<BaseApiResponse<List<Product>>>): ResponseWrapper<BaseApiResponse<List<Product>>> {
        if (_products.value?.body?.pagination?.currentPage.orDefaultInt(1) < response.body?.pagination?.currentPage.orDefaultInt(1)) {
            _products.value?.body?.data?.let { products ->
                response.body?.let {
                    return ResponseWrapper(
                        response.status,
                        BaseApiResponse(
                            code = it.code,
                            data = mutableListOf<Product>().apply {
                                addAll(products)
                                it.data?.let { addAll(it) }
                            },
                            status = it.status,
                            errors = it.errors,
                            pagination = it.pagination
                        ),
                        response.message
                    )
                }
            }
        }
        return response
    }

    override fun onCleared() {
        super.onCleared()
        searchJob?.cancel()
    }
}