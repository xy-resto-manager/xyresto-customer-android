package com.example.xyrestocustomerapp.menu.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.example.xyrestocustomerapp.R
import com.example.xyrestocustomerapp.base.adapter.BaseRvAdapter
import com.example.xyrestocustomerapp.base.model.Category
import com.example.xyrestocustomerapp.databinding.MenuCategoryItemBinding

class MenuCategoryAdapter(
    private val onSearchByCategory: (Category, Int) -> Unit,
    private val getPopularProduct: () -> Unit
): BaseRvAdapter<Category>() {

    private var activePosition: Int? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseRvViewHolder<Category> {
        val menuCategoryItemBinding = MenuCategoryItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CategoryListHolder(menuCategoryItemBinding)
    }

    override fun onBindViewHolder(holder: BaseRvViewHolder<Category>, position: Int) {
        mutableItems[position % mutableItemCount].let{
            holder.bind(it, position)
        }
    }

    inner class CategoryListHolder(
        private val binding: MenuCategoryItemBinding
    ): BaseRvViewHolder<Category>(binding.root){
        override fun bind(data: Category, position: Int?) {
            with(binding.btnCategory){
                setOnClickListener {
                    if (activePosition == position){
                        activePosition = null
                        getPopularProduct()
                    } else {
                        activePosition = position
                        position?.let { onSearchByCategory(data, it) }
                    }
                    notifyDataSetChanged()
                }
                text = data.name
                background = if (position == activePosition){
                    ContextCompat.getDrawable(context, R.drawable.button_background_yellow)
                } else {
                    ContextCompat.getDrawable(context, R.drawable.background_rectangle_yellow_2)
                }
            }
        }
    }
}