package com.example.xyrestocustomerapp.menu

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.xyrestocustomerapp.R
import com.example.xyrestocustomerapp.base.listener.PaginationListener
import com.example.xyrestocustomerapp.base.model.*
import com.example.xyrestocustomerapp.base.storage.network.ApiClient
import com.example.xyrestocustomerapp.base.utils.ResponseWrapper
import com.example.xyrestocustomerapp.base.utils.Utils
import com.example.xyrestocustomerapp.base.utils.Utils.orDefaultInt
import com.example.xyrestocustomerapp.base.widget.decoration.SpacingItemDecoration
import com.example.xyrestocustomerapp.cart.model.CartRequest
import com.example.xyrestocustomerapp.databinding.FragmentMenuBinding
import com.example.xyrestocustomerapp.home.IRetryCallBack
import com.example.xyrestocustomerapp.home.LoadingCommunicator
import com.example.xyrestocustomerapp.home.LoadingMode
import com.example.xyrestocustomerapp.home.view.HomeFragment.Companion.PRODUCT_POPULAR
import com.example.xyrestocustomerapp.home.view.IHomeActivityCommunicator
import com.example.xyrestocustomerapp.menu.adapter.MenuCategoryAdapter
import com.example.xyrestocustomerapp.menu.adapter.MenuProductAdapter
import com.example.xyrestocustomerapp.menu.network.IMenuApiService
import com.example.xyrestocustomerapp.menu.repository.MenuRepository
import com.example.xyrestocustomerapp.menu.viewmodel.MenuViewModel
import com.example.xyrestocustomerapp.menu.viewmodel.MenuViewModelFactory
import com.example.xyrestocustomerapp.search.SearchActivity

class MenuFragment : Fragment() {
    private var _viewBinding: FragmentMenuBinding? = null
    private val viewBinding get() = _viewBinding!!
    private val menuViewModel: MenuViewModel by viewModels{
        MenuViewModelFactory(MenuRepository(ApiClient.getRetrofitInstance(requireContext()).create(IMenuApiService::class.java)))
    }
    private val iHomeActivityCommunicator: IHomeActivityCommunicator by lazy { activity as IHomeActivityCommunicator }
    private val iLoadingCommunicator: LoadingCommunicator by lazy { activity as LoadingCommunicator }
    private val menuCategoryAdapter by lazy { MenuCategoryAdapter(::onCategoryButtonClicked, ::getPopularProduct) }
    private val menuProductAdapter by lazy { MenuProductAdapter(::onAddProductClicked) }
    private var totalPageProductList: Int = 1
    private var currentPageProductList: Int = 0
    private var isLastPageProductList = false
    private var isLoadingProductList = false
    private var sortBy = PRODUCT_POPULAR
    private val retryCallbackList = mutableListOf<Pair<Int, IRetryCallBack>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.let {
            it.getString(KEY_SORT_BY)?.let { sortby ->
                sortBy = sortby
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _viewBinding = FragmentMenuBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initRecyclerView()
        initObserver()
        setPaginationOnProductList()
    }

    private fun initView(){
        with(viewBinding){
            val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT)
            layoutParams.setMargins(20,0,0,25)
            layoutCategory.run {
                tvTitle.text = resources.getString(R.string.categories)
                tvTitle.layoutParams = layoutParams
                tvSubtitle.visibility = GONE
            }
            layoutProduct.run {
                tvTitle.text = resources.getString(R.string.menu)
                tvTitle.layoutParams = layoutParams
                tvSubtitle.visibility = GONE
            }
            etSearch.setOnClickListener {
                startActivity(Intent(context, SearchActivity::class.java))
            }
        }
    }

    private fun initRecyclerView(){
        with(viewBinding){
            layoutCategory.rvProductList.run {
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                adapter = menuCategoryAdapter.apply {
                    addItemDecoration(SpacingItemDecoration(leftMargin = 20))
                }
            }
            layoutProduct.rvProductList.run {
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                adapter = menuProductAdapter.apply {
                    addItemDecoration(SpacingItemDecoration(topMargin = 40, leftMargin = 35, rightMargin = 35))
                }
            }
        }
    }

    private fun initObserver(){
        iLoadingCommunicator.startLoading(2, LoadingMode.PROGRESS_BAR_WITH_BACKGROUND)
        menuViewModel.productCategories.observe(viewLifecycleOwner, {
            retryCallbackList.removeAll { it.first == 0 }
            when(it.status){
                ResponseWrapper.Status.LOADING -> {
                    Log.d("LOADING", "YES")
                }
                ResponseWrapper.Status.SUCCESS -> {
                    Log.d("DATA CATEGORY", it.body?.data.toString())
                    setCategoryList(it.body?.data)
                    iLoadingCommunicator.updateLoadingProgress()
                }
                else -> {
                    with(iLoadingCommunicator){
                        updateLoadingProgress()
                        retryCallbackList.add(Pair(0, object : IRetryCallBack{
                            override fun retry() {
                                menuViewModel.callMenuCategoryApi()
                            }
                        }))
                        if (getProgress().first == getProgress().second){
                            iLoadingCommunicator.onError(it.body?.errors.toString(), retryCallbackList)
                        }
                    }
                }
            }
        })
        menuViewModel.products.observe(viewLifecycleOwner, {
            retryCallbackList.removeAll { it.first == 1 }
            when(it.status){
                ResponseWrapper.Status.LOADING -> {
                    Log.d("LOADING", "YES")
                }
                ResponseWrapper.Status.SUCCESS -> {
                    it.body?.pagination?.let {
                        currentPageProductList = it.currentPage
                        totalPageProductList = Utils.ceilInt(
                            it.totalItems.orDefaultInt(1),
                            it.itemsPerPage.orDefaultInt(1)
                        )
                    }
                    if (currentPageProductList < totalPageProductList) {
                        it.body?.data?.let { menuProductAdapter.setItemList(it, false) }
                        menuProductAdapter.addFooter(Product(), false)
                        menuProductAdapter.notifyDataSetChanged()
                    } else {
                        isLastPageProductList = true
                        menuProductAdapter.removeFooter(false)
                        it.body?.data?.let { menuProductAdapter.setItemList(it) }
                    }
                    iLoadingCommunicator.updateLoadingProgress()
                }
                else -> {
                    with(iLoadingCommunicator){
                        updateLoadingProgress()
                        retryCallbackList.add(Pair(1, object : IRetryCallBack{
                            override fun retry() {
                                menuViewModel.callProductByCategoryApi(sort = sortBy, page = currentPageProductList+1)
                            }
                        }))
                        if (getProgress().first == getProgress().second){
                            iLoadingCommunicator.onError(it.body?.errors.toString(), retryCallbackList)
                        }
                    }
                }
            }
            isLoadingProductList = false
        })
    }

    private fun setPaginationOnProductList(){
        with(viewBinding.layoutProduct.rvProductList){
            layoutManager?.let {
                addOnScrollListener(object: PaginationListener(it, 10){
                    override fun loadMoreItems() {
                        isLoadingProductList = true
                        menuViewModel.callProductByCategoryApi(sort = sortBy, page = currentPageProductList+1)
                    }

                    override fun isLastPage(): Boolean {
                        return isLastPageProductList
                    }

                    override fun isLoading(): Boolean {
                        return isLoadingProductList
                    }

                })
            }
        }
    }

    private fun resetPaginationState(){
        totalPageProductList = 1
        currentPageProductList = 0
        isLastPageProductList = false
        isLoadingProductList = false
    }

    private fun onCategoryButtonClicked(category: Category, position: Int){
        resetPaginationState()
        sortBy = category.id.toString()
        menuProductAdapter.clearData()
        menuViewModel.callProductByCategoryApi(sort = sortBy)
    }

    private fun getPopularProduct(){
        resetPaginationState()
        sortBy = PRODUCT_POPULAR
        menuViewModel.callProductByCategoryApi(sort = sortBy)
    }

    private fun onAddProductClicked(product: Product){
        iHomeActivityCommunicator.updateCart(CartRequest(product.id, 1))
    }

    private fun setCategoryList(categoryList: List<Category>?){
        categoryList?.let { menuCategoryAdapter.setItemList(it) }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(KEY_SORT_BY, sortBy)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _viewBinding = null
    }

    companion object{
        private const val KEY_SORT_BY = "sortBy"
    }
}